﻿Array.prototype.select = function (expr) {
        var arr = this;

        switch (typeof expr) {
            case 'function':
                return $.map(arr, expr);
                break;
            case 'string':
                try {
                    var func = new Function(expr.split('.')[0], 'return ' + expr + ';');
                    return $.map(arr, func);
                }
                catch (e) {
                    return null;
                }
                break;
            default:
                throw new ReferenceError('expr not defined or not supported');
                break;
        }

    }

    Array.protorype.where = function (filter) {
        var collection = this;

        switch (typeof filter) {
            case 'function':
                return $.grep(collection, filter);
                break;
            case 'object':
                for (var property in filter) {
                    if (!filter.hasOwnProperty(prop))
                        continue;

                    collection = $.grep(collection, function (item) {
                        return item[property] === filter[property];
                    });
                }
                return collection.slice(0);
                break;
            default:
                throw new TypeError('func must be iether a function or an object of properties and values to filter by');
        }
    }

    Array.prototype.firstOrDefault = function (func) {
        return this.where(func)[0] || null;
    }