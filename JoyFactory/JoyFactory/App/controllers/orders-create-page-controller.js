﻿(function () {
    'use strict';

    angular.module('JoyFactory.controllers').controller(
        'OrdersCreatePageController',
        ['$location', 'OrdersService', 'ProgramsService', 'OrderStatusService', 'EmployerService',
            OrdersCreatePageController]);

    function OrdersCreatePageController($location, OrdersService, ProgramsService, OrderStatusService, EmployerService) {
        var vm = this;

        vm.CurrentEmployer = EmployerService.data.current;
        vm.Entertainments = ProgramsService.data;
        vm.Entity = { Id: null, EventDate: null, HeroADay: null, Timing: [{ Entertainment: ProgramsService.data.selected }] };
        vm.OrderStatuses = OrderStatusService.data;
        vm.Save = SaveOrder;

        function SaveOrder() {
            OrdersService.methods.create(vm.Entity);

            $location.path('/orders/list');
        };
    }
})();