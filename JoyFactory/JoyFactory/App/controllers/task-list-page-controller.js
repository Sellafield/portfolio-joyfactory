﻿(function () {
    'use strict'

    angular.module('JoyFactory.controllers').controller(
    'TasksListPageController',
    ['$location', 'TasksService',
        TasksListPageController]
    );

    function TasksListPageController($location, TasksService) {
        var vm = this;

        vm.TasksData = TasksService.data;
        vm.View = ViewTask;

        function ViewTask() {            
            $location.path('/tasks/view');
        }
    }
})();