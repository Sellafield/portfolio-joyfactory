﻿(function () {
    'use strict';

    angular.module('JoyFactory.controllers').controller(
        'OrdersViewPageController',
        ['$location', 'OrdersService',
            OrdersViewPageController]);

    function OrdersViewPageController($location, OrdersService) {
        var vm = this;

        vm.Edit = EditOrder;
        vm.Entity = OrdersService.data.selected;
        vm.Return = ReturnToOrderList;        

        function EditOrder(entity) {
            OrdersService.data.selected = entity;
            $location.path('/orders/edit');
        }

        function ReturnToOrderList() {
            $location.path('/orders/list');
        };
    }
})();