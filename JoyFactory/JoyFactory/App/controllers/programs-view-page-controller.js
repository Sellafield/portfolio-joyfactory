﻿(function () {
    'use strict';

    angular.module('JoyFactory.controllers').controller(
        'ProgramsViewPageController',
        ['$location', 'ProgramsService',
            ProgramsViewPageController]);

    function ProgramsViewPageController($location, ProgramsService) {
        var vm = this;

        vm.Cancel = CancelViewProgram; //sic!
        vm.Edit = EditProgram;
        vm.Entity = ProgramsService.data.selected;
        vm.Order = OrderProgram;

        function CancelViewProgram() {
            $location.path('/programs/list');
        }

        function EditProgram() {
            $location.path('/programs/edit');
        };

        function OrderProgram() {
            $location.path('/orders/create');
        };
    }
})();