﻿(function () {
    'use strict'

    angular.module('JoyFactory.controllers').controller(
        'ProgramsEditPageController',
        ['$location', 'ProgramsService', 'GendersService',
            ProgramsEditPageController]);

    function ProgramsEditPageController($location, ProgramsService, GendersService) {
        var vm = this;

        vm.Cancel = CancelEditProgram; //sic!
        vm.GendersData = GendersService.data;
        vm.Save = SaveProgram;
        vm.Entity = ProgramsService.data.selected;

        function CancelEditProgram() {
            $location.path('/programs/list');
        }

        function SaveProgram() {
            ProgramsService.methods.save(vm.Entity);
            $location.path('/programs/list');
        };
    }
})();