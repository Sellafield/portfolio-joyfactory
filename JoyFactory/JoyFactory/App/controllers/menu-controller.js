﻿(function () {
    'use strict'

    angular.module('JoyFactory.controllers').controller(
        'MenuController',
        ['$scope', '$location', 'MenuService',
            MenuController]);

    function MenuController($scope, $location, MenuService) {
        var vm = this;

        vm.MenuLinks = MenuService.data.items;

        $scope.$on('$routeChangeStart', RouteChangeHandler);

        function RouteChangeHandler(next, current) {
            var url = $location.url();

            _.each(vm.MenuLinks, function (item) {
                item.active = item.path == url;
            });
        }
    }
})();