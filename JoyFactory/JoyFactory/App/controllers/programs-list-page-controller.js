﻿(function (){
    'use strict'

    angular.module('JoyFactory.controllers').controller(
    'ProgramsListPageController',
    ['$location', 'ProgramsService', 'GendersService',
        ProgramsListPageController]
    );

    function ProgramsListPageController($location, ProgramsService, GendersService) {
        var vm = this;

        vm.Create = CreateProgram;
        vm.GendersData = GendersService.data;
        vm.ProgramsData = ProgramsService.data;
        vm.ProgramsMethods = ProgramsService.methods;
        vm.View = ViewProgram;

        function CreateProgram() {
            $location.path('/programs/create');
        }

        function ViewProgram(data) {
            vm.ProgramsData.selected = data;
            $location.path('/programs/view');
        }
    }
})();