﻿(function () {
    'use strict';

    angular.module('JoyFactory.controllers').controller(
        'AddonsViewPageController',
        ['$location', 'AddonsService',
            AddonsViewPageController]);

    function AddonsViewPageController($location, AddonsService) {
        var vm = this;

        vm.Cancel = CancelViewAddon; //sic!
        vm.Edit = EditAddon;
        vm.Entity = AddonsService.data.selected;
        vm.Order = OrderAddon;

        function CancelViewAddon() {
            $location.path('/addons/list');
        }

        function EditAddon() {
            $location.path('/addons/edit');
        };

        function OrderAddon() {
            $location.path('/orders/create');
        };        
    }
})();