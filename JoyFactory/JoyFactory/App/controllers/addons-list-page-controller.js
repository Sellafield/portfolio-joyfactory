﻿(function () {
    'use strict'

    angular.module('JoyFactory.controllers').controller(
        'AddonsListPageController',
        ['$location', 'AddonsService',
            AddonsListPageController]
        )

    function AddonsListPageController($location, AddonsService) {
        var vm = this;

        vm.Create = CreateAddon;
        vm.Data = AddonsService.data;
        vm.View = ViewAddon;

        function CreateAddon() {
            $location.path('/addons/create');
        }

        function ViewAddon(data) {
            vm.Data.selected = data;
            $location.path('/addons/view');
        }
    }
})();