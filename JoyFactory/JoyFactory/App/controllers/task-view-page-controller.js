﻿(function () {
    'use strict'

    angular.module('JoyFactory.controllers').controller(
        'TasksViewPageController', ['$location', 'TasksService', TasksViewPageController]
        );

    function TasksViewPageController($location, TasksService) {
        var vm = this;

        vm.Entity = TasksService.data.selected;

        vm.GoToOrder = GoToOrder;

        function GoToOrder() {
            $location.path('/orders/view');
        }
    }
})();