﻿(function () {
    'use strict'

    angular.module('JoyFactory.controllers').controller(
        'OrdersListPageController',
        ['$location', 'OrdersService',
            OrdersListPageController]
        )

    function OrdersListPageController($location, OrdersService) {
        var vm = this;

        vm.Create = CreateOrder;
        vm.Data = OrdersService.data;
        vm.View = ViewOrder;

        function CreateOrder(data) {
            $location.path('/orders/view');
        }

        function ViewOrder(data) {
            vm.Data.selected = data;
            $location.path('/orders/view');
        }
    }
})();