﻿(function () {
    'use strict';

    angular.module('JoyFactory.controllers').controller(
        'CustomerStatisticsViewPageController',
        ['$location', 'CustomersService',
            CustomerStatisticsViewPageController]);

    function CustomerStatisticsViewPageController($location, CustomersService) {
        var vm = this;

        //vm.Edit = EditOrder;
        vm.Entity = CustomersService.data.statistics;
        //vm.Return = ReturnToOrderList;

        //function EditOrder(entity) {
        //    OrdersService.data.selected = entity;
        //    $location.path('/orders/edit');
        //}

        //function ReturnToOrderList() {
        //    $location.path('/orders/list');
        //};
    }
})();