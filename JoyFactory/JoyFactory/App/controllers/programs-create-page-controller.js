﻿(function () {
    'use strict';

    angular.module('JoyFactory.controllers').controller(
        'ProgramsCreatePageController',
        ['$location', 'ProgramsService',
            ProgramsCreatePageController]);

    function ProgramsCreatePageController($location, ProgramsService) {
        var vm = this;

        vm.Entity = { Id: null, Name: null, Description: null, AgeLow: null, AgeHigh: null, Gender: 0, EntertainmentType: 1 };
        vm.Save = SaveProgram;

        function SaveProgram() {
            ProgramsService.methods.create(vm.Entity);
            $location.path('/programs/list');
        };
    }
})();