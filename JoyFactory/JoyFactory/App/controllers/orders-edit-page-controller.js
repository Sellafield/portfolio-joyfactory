﻿(function () {
    'use strict';

    angular.module('JoyFactory.controllers').controller(
        'OrdersEditPageController',
        ['$location', 'OrdersService', 'ProgramsService', 'AddonsService',
            OrdersEditPageController]);

    function OrdersEditPageController($location, OrdersService, ProgramsService, AddonsService) {
        var vm = this;

        vm.AddAddon = AddAddon;
        vm.AddonOrdered = AddonOrdered;
        vm.Addons = AddonsService.data.addons;
        vm.Entity = OrdersService.data.selected;
        vm.Programs = ProgramsService.data.programs
        vm.RemoveAddon = RemoveAddon;
        vm.Save = SaveOrder;

        function AddAddon(index) {
            OrdersService.methods.addAddon(index);
        }

        function AddonOrdered(addon) {
            OrdersService.methods.addonOrdered(addon, vm.Entity.Manager);
        }

        function RemoveAddon(index) {
            OrdersService.methods.removeAddon(index);
        }

        function SaveOrder() {
            OrdersService.methods.update(vm.Entity);           
            $location.path('/orders/list');
        };
    }
})();