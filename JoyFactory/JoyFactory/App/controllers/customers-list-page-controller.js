﻿(function () {
    'use strict'

    angular.module('JoyFactory.controllers').controller(
    'CustomersListPageController',
    ['$location', 'CustomersService',
        CustomersListPageController]
    );

    function CustomersListPageController($location, CustomersService) {
        var vm = this;

        //vm.Create = CreateProgram;
        //vm.GendersData = GendersService.data;
        vm.CustomersData = CustomersService.data;
        vm.CustomersMethods = CustomersService.methods;
        vm.ShowStatistics = ShowStatistics;
        //vm.View = ViewProgram;

        //function CreateProgram() {
        //    $location.path('/programs/create');
        //}

        function ShowStatistics(customerId) {
            vm.CustomersMethods.showStatistics(customerId);
            $location.path('/customers/statistics');
        }

        //function ViewProgram(data) {
        //    vm.ProgramsData.selected = data;
        //    $location.path('/programs/view');
        //}
    }
})();