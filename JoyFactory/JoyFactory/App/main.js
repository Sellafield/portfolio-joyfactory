﻿(function () {
    'use strict'

    var layouts = '/App/static/layouts/pages/';

    angular.module('JoyFactory',
        [
            'ngRoute',
            'JoyFactory.services'
        ])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/programs/list', {
                templateUrl: layouts + 'programs/programs-list-page.html',
                controller: 'ProgramsListPageController'
            });

            $routeProvider.when('/programs/view', {
                templateUrl: layouts + 'programs/programs-view-page.html',
                controller: 'ProgramsViewPageController'
            });

            $routeProvider.when('/programs/edit', {
                templateUrl: layouts + 'programs/programs-edit-page.html',
                controller: 'ProgramsEditPageController'
            });

            $routeProvider.when('/programs/create', {
                templateUrl: layouts + 'programs/programs-create-page.html',
                controller: 'ProgramsCreatePageController'
            });

            $routeProvider.when('/orders/list', {
                templateUrl: layouts + 'orders/orders-list-page.html',
                controller: 'OrdersListPageController'
            });

            $routeProvider.when('/orders/view', {
                templateUrl: layouts + 'orders/orders-view-page.html',
                controller: 'OrdersViewPageController'
            });

            $routeProvider.when('/orders/edit', {
                templateUrl: layouts + 'orders/orders-edit-page.html',
                controller: 'OrdersEditPageController'
            });

            $routeProvider.when('/orders/create', {
                templateUrl: layouts + 'orders/orders-create-page.html',
                controller: 'OrdersCreatePageController'
            });

            $routeProvider.when('/addons/list', {
                templateUrl: layouts + 'addons/addons-list-page.html',
                controller: 'AddonsListPageController'
            });

            $routeProvider.when('/addons/view', {
                templateUrl: layouts + 'addons/addons-view-page.html',
                controller: 'AddonsViewPageController'
            });

            $routeProvider.when('/customers/list', {
                templateUrl: layouts + 'customers/customers-list-page.html',
                controller: 'CustomersListPageController'
            });

            $routeProvider.when('/customers/statistics', {
                templateUrl: layouts + 'customers/customer-statistics-view-page.html',
                controller: 'CustomerStatisticsViewPageController'
            });

            $routeProvider.when('/tasks/list', {
                templateUrl: layouts + 'tasks/tasks-list-page.html',
                controller: 'TasksListPageController'
            });

            $routeProvider.when('/tasks/view', {
                templateUrl: layouts + 'tasks/tasks-view-page.html',
                controller: 'TasksViewPageController'
            });
        }]);

    console.clear();

    angular.bootstrap(document, ['JoyFactory', 'JoyFactory.controllers']);
})();