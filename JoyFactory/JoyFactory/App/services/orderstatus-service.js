﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'OrderStatusService',
        ['$http',
            OrderStatusService]
        );

    function OrderStatusService($http) {
        var service = function () { };

        service.data = {            
            statuses: null,
            
            state: null
        };

        //service.methods = {
        //    create: null,
        //    applyFilter: null,
        //    getAgesPair: null,
        //    save: null
        //}

        var init = function () {
            console.log("Statuses >> init");

            service.data.state = {
                error: false,
                message: []
            };

            getStatuses();
        };

        function getStatuses() {
            $http({
                method: 'GET',
                url: 'Statuses/GetStatuses'                
            }).success(function (data, status, headers, config) {
                if (data.length === 0)
                    setError("Статусы не найдены");
                service.data.statuses = data;
            }).error(function (data, status, headers, config) {
                service.data.state.error = setError(data.message);
                showError();
            });            
        }

        var setError = function (message) {
            service.data.state.message.push(message);
            return true;
        }

        var showError = function () {
            if (true)
                _.each(service.data.state.message, function (value) {
                    console.log("ObjTreeService >> " + value);
                });
        };

        init();

        return service;
    }
})();