﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'MenuService',
            MenuService
        );

    function MenuService() {
        var service = function () { };

        service.data = {
            items: [
            {
                text: 'Заказ-Заявки',
                active: false,
                path: '#orders/list'
            },
            {
                text: 'Клиенты',
                active: false,
                path: '#customers/list'
            },
            {
                text: 'Программы',
                active: false,
                path: '#programs/list'
            },
            {
                text: 'Добавки',
                active: false,
                path: '#addons/list'
            },
            {
                text: 'Мои задачи',
                active: false,
                path: '#tasks/list'
            }
            ]
        };

        return service;
    }
})();