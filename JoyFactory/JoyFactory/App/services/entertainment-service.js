﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'EntertainmentsService',
        ['$http',
            EntertainmentsService]
        );

    function EntertainmentsService($http) {
        var service = function () { };

        service.data = {
            selected: null,
            entertainments: null,

            state: null
        };

        var init = function () {
            console.log("Entertainments >> init");

            service.data.state = {
                error: false,
                message: []
            };

            getEntertainments();
        };

        function getEntertainments() {
            $http({
                method: 'GET',
                url: 'Entertainments/GetEntertainments',                        
            }).success(function (data, status, headers, config) {
                if (data.length === 0)
                    setError("Услуги не найдены");
                service.data.entertainemnts = data;
            }).error(function (data, status, headers, config) {
                service.data.state.error = setError(data.message);
                showError();
            });
        }

        var setError = function (message) {
            service.data.state.message.push(message);
            return true;
        }

        var showError = function () {
            if (true)
                _.each(service.data.state.message, function (value) {
                    console.log("ObjTreeService >> " + value);
                });
        };

        init();

        return service;
    }
})();