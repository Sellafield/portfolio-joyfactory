﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'CustomersService',
        ['$http', 'OrdersService',
            CustomersService]
        );

    function CustomersService($http, OrdersService) {
        var service = function () { };

        service.data = {
            customers: null,
            selected: null,
            statistics: null
        };

        service.methods = {
            search: null,
            create: null,
            edit: null,
            showStatistics: null,
            customerorders: null,
        };

        var init = function () {
            console.log("Clients >> init");

            service.data.state = {
                error: false,
                message: []
            };

            getCustomers();
        };

        function getCustomers() {
            var dummyData = [
                {
                    "Customer": {
                        "Id": 1,
                        "FirstName": "Анатолий",
                        "LastName": "Вассерман",
                        "Phone": "123-45-67"
                    },
                    "CustomerOrders": [
                        {
                            "OrderId": 1,
                            "OrderStatus": {
                                "StatusId": 1,
                                "EvaluationId": 1,
                                "Comment": "Просто очень ужасная музыка"
                            }
                        },
                        {
                            "OrderId": 2,
                            "OrderStatus": {
                                "StatusId": 3,
                                "CancelReasonId": 1
                            }
                        }
                    ]
                },
                {
                    "Customer": {
                        "Id": 2,
                        "FirstName": "Александр",
                        "LastName": "Друзь",
                        "Phone": "321-54-76"
                    },
                    "CustomerOrders": [
                        {
                            "OrderId": 3,
                            "OrderStatus": {
                                "StatusId": 3,
                                "CancelReasonId": 2,
                            }
                        }
                    ]
                },
                {
                    "Customer": {
                        "Id": 3,
                        "FirstName": "Ирада",
                        "LastName": "Зейналова",
                        "Phone": "111-22-33"
                    },
                    "CustomerOrders": [
                        {
                            "OrderId": 4,
                            "OrderStatus": {
                                "StatusId": 1,
                                "EvaluationId": 1,
                                "Comment": "Просто очень ужасная музыка"
                            }
                        }
                    ]
                }
            ];

            service.data.customers = dummyData;
        }

        function showStatistics(customerId) {
            var dummyStatistics = {
                "Customer": {
                    "Id": 1,
                    "FirstName": "Анатолий",
                    "LastName": "Вассерман",
                    "Phone": "123-45-67"
                },
                "GoodOrders": [
                    {
                        "ProgramName": "Черепашки Ниндзя",
                        "Evaluation": "Отлично"
                    },
                    {
                        "ProgramName": "Кукла-Шоу",
                        "Evaluation": "Хорошо"
                    }
                ],
                "BadOrders": [
                    {
                        "ProgramName": "Шарах-Шоу",
                        "CancelReason": "Изменились планы"
                    }
                ]
            };

            //var customer = service.data.customers.toArray().firstOrDefault(c=>c.Customer.Id == customerId);

            //var customerOrders = customer.CustomerOrders;

            //var goodOrdersIds = customerOrders.where(co=>co.OrderStatus.StatusId == 1).select('x.OrderId'); //.find(co=>co.OrderStatus.StatusId == 1)

            //goodOrdersIds.forEach(function (x) { console.log(x) });

            //var badOrders = customer.CustomerOrders.find(co=>co.OrderStatus.StatusId == 3);

            //var statistics = { "Customer": customer.Customer, "GoodOrders": goodOrders, "BadOrders": badOrders };

            service.data.statistics = dummyStatistics;
        }

        service.methods.showStatistics = function (customerId) {
            showStatistics(customerId);
        }

        init();

        return service;
    }
})();

//function getCustomers() {
    //$http({
    //    method: 'GET',
    //    url: 'Programs/GetPrograms',
    //    params: {
    //        agefilter: service.data.agefilter,
    //        genderfilter: service.data.genderfilter
    //    }
    //}).success(function (data, status, headers, config) {
    //    if (data.length === 0)
    //        setError("Программы не найдены");
    //    service.data.programs = data;
    //}).error(function (data, status, headers, config) {
    //    service.data.state.error = setError(data.message);
    //    showError();
    //});
//}