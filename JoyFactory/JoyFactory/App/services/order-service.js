﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'OrdersService',
        ['$http',
            OrdersService]
        );

    function OrdersService($http) {
        var service = function () { };

        service.data = {
            selected: null,
            orders: null,

            createnew: false,

            state: null
        };

        service.methods = {
            addAddon: null,
            create: null,
            removeAddon: null,
            update: null,
            addonOrdered: null
        }

        var init = function () {
            console.log("Orders >> init");

            service.data.state = {
                error: false,
                message: []
            };

            getOrders();
        };

        service.methods.addAddon = addAddon;

        service.methods.addonOrdered = function (addon, manager) {
            addonOrdered(addon, manager);
        }

        service.methods.create = function (entity) {
            createOrder(entity);
        }

        service.methods.removeAddon = removeAddon;

        service.methods.update = function (entity) {
            updateOrder(entity);
        }

        var getOrders = function () {
            $http({
                method: 'GET',
                url: 'Orders/GetOrders',                        
            }).success(function (data, status, headers, config) {
                if (data.length === 0)
                    setError("Заявки не найдены");
                service.data.orders = data;
            }).error(function (data, status, headers, config) {
                service.data.state.error = setError(data.message);
                showError();
            });            
        }

        function addAddon(index) {
            service.data.selected.Addons.splice(index + 1, 0, {
                "Addon": {
                    "Id": 0,
                    "Name": ""
                },
                "Status": "Не заказано",
                "Manager": null
            })
        }

        var addonOrdered = function (addon, manager) {
            addon.Status = "Заказано";
            addon.Manager = manager;
        }

        var createOrder = function (entity) {

            $http({
                method: 'POST',
                url: '/Orders/CreateOrder',                        
                data: entity
            }).success(function (data, status, headers, config) {
                getOrders();
            }).error(function (data, status, headers, config) {

                service.data.state.error = setError(data.message);

            });
        };

        function removeAddon(index) {
            var addonInd = 0;
            service.data.selected.Addons.splice(index, 1)
        }

        var updateOrder = function (entity) {

            $http({
                method: 'POST',
                url: '/Orders/UpdateOrder',
                params: entity
            }).success(function (data, status, headers, config) {
                        
            }).error(function (data, status, headers, config) {

                service.data.state.error = setError(data.message);

            });
        };

        var setError = function (message) {
            service.data.state.message.push(message);
            return true;
        }

        var showError = function () {
            if (true)
                _.each(service.data.state.message, function (value) {
                    console.log("ObjTreeService >> " + value);
                });
        };

        init();

        return service;
    }
})();