﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'TasksService',
        ['$http',
            TasksService]
        );

    function TasksService($http) {
        var service = function () { };

        service.data = {
            tasks: null,
            selected: null
        };

        service.methods = {
            search: null,
            create: null,
            edit: null,
            view: null,            
        };

        var init = function () {
            console.log("Tasks >> init");

            service.data.state = {
                error: false,
                message: []
            };

            getTasks();
            view();
        };

        function getTasks() {
            var dummyData = [
                {                    
                    "Id": 1,
                    "Name": "Заказать добавки",
                    "RelatedProgram": {
                        "Id": 1,
                        "Name": "Кукла-Шоу"
                    },
                    "Deadline": "26.01.2016"
                },
                {
                    "Id": 2,
                    "Name": "Расставить людей",
                    "RelatedProgram": {
                        "Id": 1,
                        "Name": "Кукла-Шоу"
                    },
                    "Deadline": "28.01.2016"
                },
                {
                    "Id": 3,
                    "Name": "Заказать добавки",
                    "RelatedProgram": {
                        "Id": 2,
                        "Name": "Черепашки-Ниндзя"
                    },
                    "Deadline": "25.01.2016"
                },
                {
                    "Id": 4,
                    "Name": "Подготовить смету",
                    "RelatedProgram": {
                        "Id": 3,
                        "Name": "Пиратская сказка"
                    },
                    "Deadline": "29.01.2016"
                },
                {
                    "Id": 5,
                    "Name": "Сделать контрольный звонок",
                    "RelatedProgram": {
                        "Id": 4,
                        "Name": "Красная Шапочка"
                    },
                    "Deadline": "28.01.2016"
                },
            ];

            service.data.tasks = dummyData;
        }

        function view() {
            var dummy = {
                "Id": 1,
                "Name": "Заказать добавки",
                "RelatedProgram": {
                    "Id": 1,
                    "Name": "Кукла-Шоу"
                },
                "Deadline": "26.01.2016"
            };

            service.data.selected = dummy;
        }

        service.data.selected = {
            "Id": 2,
            "Name": "Расставить людей",
            "RelatedProgram": {
                "Id": 1,
                "Name": "Кукла-Шоу"
            },
            "Deadline": "28.01.2016"
        };

        service.methods.view = function () {
            view();
        }

        init();

        return service;
    }
})();