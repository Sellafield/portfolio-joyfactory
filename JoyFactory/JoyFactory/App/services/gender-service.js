﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'GendersService',
        ['$http',
            GendersService]
        );

    function GendersService($http) {
        var service = function () { };

        service.data = {
            selected: null,

            genders: null,

            state: null
        };

        var init = function () {
            console.log("Genders >> init");

            service.data.state = {
                error: false,
                message: []
            };

            getGenders();
        };

        var getGenders = function () {

            $http({
                method: 'GET',
                url: 'Entertainments/GetGenders',
            }).success(function (data, status, headers, config) {
                if (data.length === 0)
                    setError("Полы не найдены");
                service.data.genders = data;
            }).error(function (data, status, headers, config) {
                service.data.state.error = setError(data.message);
                showError();
            });

            //var dummyData = [
            //    { "Id": 0, "Name": "Для всех" },
            //    { "Id": 1, "Name": "Для мальчиков" },
            //    { "Id": 2, "Name": "Для девочек" }
            //];

            //service.data.genders = dummyData;
        }

        var setError = function (message) {
            service.data.state.message.push(message);

            return true;
        }

        var showError = function () {
            if (true)
                _.each(service.data.state.message, function (value) {
                    console.log("ObjTreeService >> " + value);
                });
        };

        init();

        return service;
    }
})();