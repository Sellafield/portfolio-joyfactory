﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'AddonsService',
        ['$http',
            AddonsService]
        );

    function AddonsService($http) {
        var service = function () { };

        service.data = {
            selected: null,
            addons: null,

            state: null
        };

        service.methods = {
            create: null
        }

        var init = function () {
            console.log("Addons >> init");

            service.data.state = {
                error: false,
                message: []
            };

            getAddons();
        };

        function getAddons() {
            //$http({
            //    method: 'GET',
            //    url: 'Addons/GetAddons'                        
            //}).success(function (data, status, headers, config) {
            //    if (data.length === 0)
            //        setError("Добавки не найдены");
            //    service.data.addons = data;
            //}).error(function (data, status, headers, config) {
            //    service.data.state.error = setError(data.message);
            //    showError();
            //});

            var dummyData = [
                { "Id": 1, "Name": "Заказ торта", "Description": "Закажите имениннику праздничный торт", "EntertainmentType": 2 },
                { "Id": 2, "Name": "Украшение шарами", "Description": "Украшение праздничной площадки яркими шарами", "EntertainmentType": 2 },
                { "Id": 3, "Name": "Дрессированные животные", "Description": "Выступления дрессированных животных украсят любой праздник", "EntertainmentType": 2}
            ];

            service.data.addons = dummyData;
        }

        function Create(entity) {
            $http({
                method: 'POST',
                url: '/Addons/CreateAddon',
                data: entity
            }).success(function (data, status, headers, config) {
                getAddons();
            }).error(function (data, status, headers, config) {

                service.data.state.error = setError(data.message);

            });
        }

        function getAgesPair(ageLow, ageHigh) {
            return ageLow + " - " + ageHigh + " лет";
        }

        service.methods.create = function (entity) {
            Create(entity);
        }                

        service.methods.getAgesPair = function (ageLow, ageHigh) {
            getAgesPair(ageLow, ageHigh);
        }

        var setError = function (message) {
            service.data.state.message.push(message);
            return true;
        }

        var showError = function () {
            if (true)
                _.each(service.data.state.message, function (value) {
                    console.log("ObjTreeService >> " + value);
                });
        };

        init();

        return service;
    }
})();