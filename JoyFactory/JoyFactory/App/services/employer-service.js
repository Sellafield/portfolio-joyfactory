﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'EmployerService',
        ['$http',
            EmployerService]
        );

    function EmployerService($http) {
        var service = function () { };

        service.data = {
            current: null,
            employers: null,
            selected: null
        };

        var init = function () {
            console.log('Employers >> init');

            service.data.state = {
                error: false,
                message: []
            };

            getEmployers();
        }

        var getEmployers = function() {
            $http({
                method: 'GET',
                url: 'Employer/GetDummyEmployer',
            }).success(function (data, status, headers, config) {
                if (data.length === 0)
                    setError("Сотрудники не найдены");
                service.data.current = data;
            }).error(function (data, status, headers, config) {
                service.data.state.error = setError(data.message);
                showError();
            });
        }

        var setError = function (message) {
            service.data.state.message.push(message);

            return true;
        }

        var showError = function () {
            if (true)
                _.each(service.data.state.message, function (value) {
                    console.log("ObjTreeService >> " + value);
                });
        };

        init();

        return service;
    }
})();