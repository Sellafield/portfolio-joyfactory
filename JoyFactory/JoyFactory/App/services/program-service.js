﻿(function () {
    'use strict'

    angular.module('JoyFactory.services').factory(
        'ProgramsService',
        ['$http',
            ProgramsService]
        );

    function ProgramsService($http) {
        var service = function () { };

        service.data = {
            selected: null,
            programs: null,

            agefilter: null,
            genderfilter: null,

            state: null
        };

        service.methods = {
            create:  null,
            applyFilter: null,
            getAgesPair: null,
            save: null
        }

        var init = function () {
            console.log("Programs >> init");

            service.data.state = {
                error: false,
                message: []
            };

            getPrograms();
        };

        function getPrograms() {
            $http({
                method: 'GET',
                url: 'Entertainments/GetEntertainments',
                params: {
                    agefilter: service.data.agefilter,
                    genderfilter: service.data.genderfilter
                }
            }).success(function (data, status, headers, config) {
                if (data.length === 0)
                    setError("Программы не найдены");
                service.data.programs = data;
            }).error(function (data, status, headers, config) {
                service.data.state.error = setError(data.message);
                showError();
            });

            //var dummyData = [
            //    { "AgeLow": 1, "AgeHigh": 3, "Gender": 0, "Id": 1, "Name": "Красная Шапочка", "Description": "Красная Шапочка проведет для вашего ребенка серию игр про торт", "EntertainmentType": 1 },
            //    { "AgeLow": 2, "AgeHigh": 5, "Gender": 1, "Id": 2, "Name": "Винни-Пух и все-все-все", "Description": "Кукольный спектакль с участием героев знаменитого советского мультфильма", "EntertainmentType": 1 },
            //    { "AgeLow": 4, "AgeHigh": 5, "Gender": 2, "Id": 3, "Name": "Золотой ключик", "Description": "Мальвина и Буратино снова ищут Золотой Ключик", "EntertainmentType": 1 }
            //];

            //service.data.programs = dummyData;
        }

        function Create(entity) {
            $http({
                method: 'POST',
                url: '/Entertainments/CreateEntertainment',
                data: entity
            }).success(function (data, status, headers, config) {
                getPrograms();
            }).error(function (data, status, headers, config) {

                service.data.state.error = setError(data.message);

            });
        }

        function Save(entity) {
            $http({
                method: 'POST',
                url: '/Entertainments/SaveEntertainment',
                data: entity
            }).success(function (data, status, headers, config) {
                getPrograms();
            }).error(function (data, status, headers, config) {

                service.data.state.error = setError(data.message);

            });
        }

        function getAgesPair(ageLow, ageHigh) {
            return ageLow + " - " + ageHigh + " лет";
        }

        service.methods.create = function (entity) {
            Create(entity);
        }

        service.methods.applyFilter = function () {
            getPrograms();                    
        }

        service.methods.getAgesPair = function (ageLow, ageHigh) {
            getAgesPair(ageLow, ageHigh);
        }

        service.methods.save = function (entity) {
            Save(entity);
        }
                
        var setError = function (message) {
            service.data.state.message.push(message);
            return true;
        }

        var showError = function () {
            if (true)
                _.each(service.data.state.message, function (value) {
                    console.log("ObjTreeService >> " + value);
                });
        };

        init();

        return service;
    }
})();