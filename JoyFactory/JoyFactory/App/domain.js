﻿(function () {
    'use strict'

    angular.module('JoyFactory.directives', []);

    angular.module('JoyFactory.services', []);

    angular.module('JoyFactory.controllers', ['JoyFactory.services', 'JoyFactory.directives']);
})();