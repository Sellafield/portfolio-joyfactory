﻿//http://onehungrymind.com/angularjs-dynamic-templates/
(function () {
    console.log('Directives >> load')
    angular.module('JoyFactory.directives').directive('agesPair', function ($compile) {
        var linker = function (scope, element, attrs) {
            var ageWord = null;

            var lastNumber = scope.entity.AgeHigh.toString().slice(-1);

            console.log(scope.entity.AgeHigh);
            console.log(lastNumber);

            switch (lastNumber) {
                case '1':
                    {
                        ageWord = "год";
                        break;
                    }
                case '2':
                case '3':
                case '4':
                    {
                        ageWord = "года";
                        break;
                    }
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '0': {
                    ageWord = "лет";
                    break;
                }
            }

            var text = '<span>{{entity.AgeLow}} - {{entity.AgeHigh}} ' + ageWord + '</span>'

            element.html(text).show();

            $compile(element.contents())(scope);
        }

        return {
            restrict: 'E',
            link: linker,
            scope: {
                entity: '='
            }            
        };        
    });
})(); 