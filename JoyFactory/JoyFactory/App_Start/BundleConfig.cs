﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace JoyFactory.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/App/static/css/")
                .Include("~/App/static/css/layout.css")
                .Include("~/Content/bootstrap.css"));

            bundles.Add(new ScriptBundle("~/App/static/js/")
                .Include("~/Scripts/jquery-1.10.2.js")
                .Include("~/Scripts/jquery-ui-1.11.4.js")

                .Include("~/Scripts/angular.js")
                .Include("~/Scripts/angular-scenario.js")
                .Include("~/Scripts/angular-route.js")
                .Include("~/Scripts/angilar-mocks.js")

                .Include("~/Scripts/underscore.js")

                .Include("~/Scripts/bootstrap.js")

                //.Include("~/Scripts/extensions/array.js")

                .Include("~/App/domain.js")

                .Include("~/App/services/*.js")
                .Include("~/App/controllers/*.js")
                .Include("~/App/directives/*.js")

                .Include("~/App/main.js"));
        }
    }
}