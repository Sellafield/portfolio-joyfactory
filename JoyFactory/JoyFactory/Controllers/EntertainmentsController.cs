﻿using JoyFactory.Controllers.Base;
using JoyFactory.Models.Enums;
using JoyFactory.Models.SubmitModels;
using Models.SubmitModels;
using Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JoyFactory.Controllers
{
    public class EntertainmentsController : BaseController
    {
        public JsonResult GetAllEntertainments()
        {
            var service = (IEntertainmentsService)(kernel.GetService(typeof(IEntertainmentsService)));

            return Json(service.GetEntertainments(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGenders()
        {
            var service = (IEntertainmentsService)(kernel.GetService(typeof(IEntertainmentsService)));

            return Json(service.GetGendersList(), JsonRequestBehavior.AllowGet);
        }        

        public JsonResult GetEntertainments(int agefilter = 0, int genderfilter = (int)Gender.Both)
        {
            var service = (IEntertainmentsService)(kernel.GetService(typeof(IEntertainmentsService)));

            return Json(service.GetEntertainmentsBy((agelow, agehigh, gender) => ((agefilter == 0 || (agelow <= agefilter && agehigh >= agefilter)) && (genderfilter == (int)Gender.Both || (int)gender == genderfilter))), JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateEntertainment(EntertainmentSubmitModel entity)
        {
            var service = (IEntertainmentsService)(kernel.GetService(typeof(IEntertainmentsService)));

            return Json(service.CreateEntertainment(entity), JsonRequestBehavior.AllowGet);
        }
    }
}