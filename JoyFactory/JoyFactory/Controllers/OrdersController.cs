﻿using JoyFactory.Controllers.Base;
using JoyFactory.Shared.Interfaces;
using Models.SubmitModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JoyFactory.Controllers
{
    public class OrdersController : BaseController
    {
        public JsonResult GetOrders()
        {
            var service = (IOrderService)(kernel.GetService(typeof(IOrderService)));

            return Json(service.GetAllOrders(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateOrder(OrderSubmitModel entity)
        {
            var service = (IOrderService)(kernel.GetService(typeof(IOrderService)));

            return Json(service.UpdateOrder(entity), JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateOrder(OrderSubmitModel entity)
        {
            var service = (IOrderService)(kernel.GetService(typeof(IOrderService)));

            return Json(service.CreateOrder(entity), JsonRequestBehavior.AllowGet);
        }
    }
}