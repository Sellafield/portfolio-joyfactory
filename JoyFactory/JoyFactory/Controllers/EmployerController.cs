﻿using JoyFactory.Controllers.Base;
using JoyFactory.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JoyFactory.Controllers
{
    public class EmployerController : BaseController
    {
        public JsonResult GetDummyEmployer()
        {
            var service = (IEmployerService)(kernel.GetService(typeof(IEmployerService)));

            return Json(service.GetEmployers().FirstOrDefault(), JsonRequestBehavior.AllowGet);
        }
    }
}