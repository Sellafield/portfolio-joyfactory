﻿using JoyFactory.Controllers.Base;
using JoyFactory.Shared.Interfaces;
using Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JoyFactory.Controllers
{
    public class StatusesController : BaseController
    {
        public JsonResult GetStatuses()
        {
            var service = (IOrderStatusService)(kernel.GetService(typeof(IOrderStatusService)));

            return Json(service.GetStatuses(), JsonRequestBehavior.AllowGet);
        }
    }
}