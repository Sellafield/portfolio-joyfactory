﻿using JoyFactory.Shared.Interfaces;
using Models.ViewModels;
using Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Services
{
    public class ProgramService
    {
        private IProgramRepository repository;

        public ProgramService(IProgramRepository repo)
        {
            repository = repo;
        }

        public List<ProgramViewModel> GetAllPrograms()
        {
            var entities = repository.GetAll();
            return entities.Select(e=>(ProgramViewModel)e).ToList();
        }
    }
}
