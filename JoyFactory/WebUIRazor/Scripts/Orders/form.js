﻿$(function () {
    $('#Customer').autocomplete({
        serviceUrl: '/customers/search',
        minChars: 3,
        onSelect: function (suggestion) {
            $("#customerPlaceholder").empty();
            $("#customerPlaceholder").append('<label>'+suggestion.value+'</label>');
            $("#CustomerId").val(suggestion.data);            
        },
        appendTo: $('#cust_ac_placeholder')
    });
});

function addEntertainment() {
    var entList = $('#entHolder');
    $.get('/Orders/AddEntertainment', function (result) {
        entList.append(result);
    });
    return false;
}

function selectMainEnt(sender) {
    $(".entertainment").each(function (id, el) {
        $(el).prop("checked", false);
    });
    $(sender).prop("checked", true);
}