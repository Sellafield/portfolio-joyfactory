﻿using JoyFactory.Models.SubmitModels;
using JoyFactory.Models.ViewModels;
using Models.SubmitModels;
using Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUIRazor.Controllers
{
    public class EntertainmentsController : BaseController
    {
        IEntertainmentsService service;

        public EntertainmentsController()
        {
            service = (IEntertainmentsService)(kernel.GetService(typeof(IEntertainmentsService)));
        }

        public ActionResult Create()
        {
            return View(new EntertainmentViewModel());
        }

        // GET: Entertainments
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            var entertainments = service.GetEntertainments();

            return View(entertainments);
        }

        public ActionResult View(int? Id)
        {
            var entertainment = service.GetEntertainment(Id.GetValueOrDefault(0));

            return View(entertainment);
        }

        public ActionResult Edit(int Id)
        {
            var entertainment = service.GetEntertainment(Id);

            return View(entertainment);
        }

        public ActionResult Save(EntertainmentSubmitModel submitModel)
        {
            long result = 0;

            if (submitModel.Id > 0)
            {
                result = service.UpdateEntertainment(submitModel);
            }
            else
            {
                result = service.CreateEntertainment(submitModel);
            }

            return RedirectToAction("View", "Entertainments", new { Id = result });
        }


        [HttpPost]
        public ActionResult Delete(int Id)
        {
            service.DeleteEntertainment(Id);

            return RedirectToAction("List", "Entertainments");
        }
    }
}