﻿using JoyFactory.Models.SubmitModels;
using JoyFactory.Shared.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUIRazor.Controllers
{
    public class CustomersController : BaseController
    {
        ICustomerService service;

        public CustomersController()
        {
            service = (ICustomerService)(kernel.GetService(typeof(ICustomerService)));
        }

        // GET: Customers
        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            var customers = service.GetCustomers();

            return View(customers);
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            var entity = service.GetCustomer(id);

            return View(entity);
        }

        public ActionResult Save(CustomerSubmitModel entity)
        {
            var result = 0;

            if (entity.Id > 0)
            {
                result = service.UpdateCustomer(entity);
            } else
            {
                result = service.CreateCustomer(entity);
            }

            return RedirectToAction("View", "Customers", new { Id = result });
        }

        public ActionResult View(int id)
        {
            var entity = service.GetCustomer(id);

            return View(entity);
        }

        public ActionResult Search()
        {
             var searchString = Request["query"];

            var suggestions = service.GetCustomerByName(searchString).Select(r=>new { value = String.Format("{0} {1} {2}", r.LastName, r.FirstName, r.MiddleName), data = r.Id }).ToArray();

            var result = new { suggestions = suggestions };
            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int Id)
        {
            service.DeleteCustomer(Id);

            return RedirectToAction("List", "Customers");
        }
    }
}