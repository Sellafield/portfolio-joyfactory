﻿using JoyFactory.Models.ViewModels;
using JoyFactory.Shared.Interfaces;
using Models.SubmitModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUIRazor.Controllers
{
    public class OrdersController : BaseController
    {
        IOrderService ordersService;
        ICustomerService customersService;

        public OrdersController()
        {
            ordersService = (IOrderService)(kernel.GetService(typeof(IOrderService)));
            customersService = (ICustomerService)(kernel.GetService(typeof(ICustomerService)));
        }

        // GET: Orders
        public ActionResult Index()
        {
            return View("List");
        }

        public ActionResult List()
        {
            var orders = ordersService.GetAllOrders();            

            return View(orders);
        }

        public ActionResult View(int id)
        {
            var entity = ordersService.GetOrder(id);

            return View(entity);
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Save(OrderSubmitModel entity)
        {
            var result = 0;

            if (entity.Id > 0)
            {
                result = ordersService.UpdateOrder(entity);
            }
            else
            {
                entity.OrderDate = DateTime.Now;     
                result = ordersService.CreateOrder(entity);
            }

            return RedirectToAction("View", "Orders", new { Id = result });
        }

        [HttpPost]
        public ActionResult Delete(int Id)
        {
            ordersService.DeleteOrder(Id);

            return RedirectToAction("List", "Orders");
        }

        public ActionResult Edit(int id)
        {
            var entity = ordersService.GetOrder(id);

            return View(entity);
        }

        public ActionResult AddEntertainment()
        {
            var entity = new TimingRecordViewModel();

            return View("~/Views/Shared/Entertainments/Entertainment.cshtml", entity);
        }
    }
}