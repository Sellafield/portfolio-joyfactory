﻿using JoyFactory.App_Start;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUIRazor.Controllers
{
    public class BaseController : Controller
    {
        public IKernel kernel
        {
            get { return NinjectConfig.GetConfiguredKernel(); }
        }
    }
}