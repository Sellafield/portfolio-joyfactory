﻿using JoyFactory.Domain.DataAccess.Base;
using JoyFactory.Models.DataBase;
using JoyFactory.Storage.Interfaces;
using Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Domain.DataAccess
{
    public class EntertainmentsRepository : BaseRepository<Entertainment>, IEntertainmentsRepository
    {
        public EntertainmentsRepository(IJoyFactoryContextFactory databaseFactory) : base(databaseFactory)
        {

        }
    }
}
