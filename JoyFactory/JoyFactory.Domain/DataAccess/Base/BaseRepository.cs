﻿using JoyFactory.Storage;
using JoyFactory.Storage.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Domain.DataAccess.Base
{
    public abstract class BaseRepository<T>: ReadOnlyRepository<T> where T : class
    {
        protected BaseRepository(IJoyFactoryContextFactory factory) : base (factory)
        {
            
        }

        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
            dataContext.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            dataContext.Entry(entity).State = EntityState.Modified;
            dataContext.SaveChanges();
        }

        public virtual void Delete(int id)
        {
            var entity = GetById(id);

            if (entity == null)
                return;

            dbSet.Remove(entity);
            dataContext.SaveChanges();
        }

        public void Delete(Func<T, bool> condition)
        {
            var entities = dbSet.Where(condition).AsEnumerable();
            foreach (T entity in entities)
                dbSet.Remove(entity);
            dataContext.SaveChanges();
        }
    }
}
