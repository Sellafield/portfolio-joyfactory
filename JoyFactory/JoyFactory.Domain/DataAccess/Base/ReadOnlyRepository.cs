﻿using JoyFactory.Storage;
using JoyFactory.Storage.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Domain.DataAccess.Base
{
    public abstract class ReadOnlyRepository<T> where T : class
    {
        protected readonly IDbSet<T> dbSet;

        protected JoyFactoryContext dataContext;

        protected ReadOnlyRepository(IJoyFactoryContextFactory factory)
        {
            DatabaseContextFactory = factory;
            dbSet = DataContext.Set<T>();
        }

        protected IJoyFactoryContextFactory DatabaseContextFactory { get; private set; }

        protected JoyFactoryContext DataContext
        {
            get
            {
                return dataContext ?? (dataContext = DatabaseContextFactory.Get());
            }
        }

        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }

        public virtual IEnumerable<T> GetPage<TKey>(int rowOffset, int rows, Func<T, TKey> sortSelector, int sortOrder)
        {
            return (sortOrder > 0 ? dbSet.OrderBy(sortSelector) : dbSet.OrderByDescending(sortSelector)).Skip(rowOffset).Take(rows);
        }

        public virtual IEnumerable<T> GetMany(Func<T, bool> condition)
        {
            return dbSet.Where(condition).ToList();
        }

        public T GetByCondition(Func<T, bool> condition)
        {
            return dbSet.Where(condition).FirstOrDefault();
        }
    }
}
