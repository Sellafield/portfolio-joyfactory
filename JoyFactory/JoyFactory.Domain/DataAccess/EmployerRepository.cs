﻿using JoyFactory.Domain.DataAccess.Base;
using JoyFactory.Models.DataBase;
using JoyFactory.Shared.Interfaces;
using JoyFactory.Storage.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Domain.DataAccess
{
    public class EmployerRepository : ReadOnlyRepository<Employer>, IEmployerRepository
    {
        public EmployerRepository(IJoyFactoryContextFactory databaseFactory) : base(databaseFactory) { }
    }
}
