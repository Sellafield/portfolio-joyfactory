﻿using JoyFactory.Domain.DataAccess.Base;
using JoyFactory.Models.DataBase;
using JoyFactory.Shared.Interfaces;
using JoyFactory.Storage.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Domain.DataAccess
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(IJoyFactoryContextFactory databaseFactory) : base(databaseFactory) { }

        public override void Add(Order entity)
        {
            foreach (var timingRecord in entity.Timing)
            {
                if (timingRecord.Entertainment != null)
                    DataContext.Entry(timingRecord.Entertainment).State = EntityState.Unchanged;
            }            

            base.Add(entity);
        }
    }
}
