﻿using JoyFactory.Models.ViewModels;
using JoyFactory.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Domain.Services
{
    public class EmployerService : IEmployerService
    {
        private readonly IEmployerRepository repository;

        public EmployerService(IEmployerRepository repositoryParam)
        {
            repository = repositoryParam;
        }

        public List<EmployerViewModel> GetEmployers()
        {
            var entities = repository.GetAll();
            return entities.Select(e => (EmployerViewModel)e).ToList();
        }

        public EmployerViewModel GetEmployerById(int id)
        {
            var entity = repository.GetById(id);
            return entity != null ? (EmployerViewModel)entity : null;
        }
    }
}
