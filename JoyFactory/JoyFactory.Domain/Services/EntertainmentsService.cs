﻿using JoyFactory.Models.DataBase;
using JoyFactory.Models.Enums;
using JoyFactory.Models.SubmitModels;
using JoyFactory.Models.ViewModels;
using Models.SubmitModels;
using Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Domain.Services
{
    public class EntertainmentsService : IEntertainmentsService
    {
        private readonly IEntertainmentsRepository repository;

        public EntertainmentsService(IEntertainmentsRepository repositoryParam)
        {
            repository = repositoryParam;
        }

        public List<EntertainmentViewModel> GetEntertainments()
        {
            var entities = repository.GetAll();
            return entities.OrderBy(e => e.AgeLow).ThenBy(e => e.AgeHigh).ThenBy(e => e.Name).Select(e => (EntertainmentViewModel)e).ToList();
        }

        public List<EntertainmentViewModel> GetEntertainments(EntertainmentType type)
        {
            var entities = repository.GetMany(e=>e.Type == type);
            return entities.OrderBy(e => e.AgeLow).ThenBy(e => e.AgeHigh).ThenBy(e => e.Name).Select(e => (EntertainmentViewModel)e).ToList();
        }

        public List<EntertainmentViewModel> GetEntertainmentsBy(Func<int, int, Gender, bool> condition)
        {
            var entities = repository.GetMany(e => condition(e.AgeLow, e.AgeHigh, e.Gender));
            return entities.OrderBy(e => e.AgeLow).ThenBy(e => e.AgeHigh).ThenBy(e => e.Name).Select(e => (EntertainmentViewModel)e).ToList();
        }

        public EntertainmentViewModel GetEntertainment(int id)
        {
            var entity = repository.GetById(id);
            return entity != null ? (EntertainmentViewModel)entity : null;
        }

        public int CreateEntertainment(EntertainmentSubmitModel entity)
        {
            int result = 0;

            try
            {
                var newEntity = (Entertainment)entity;

                repository.Add(newEntity);

                result = newEntity.Id;
            }
            catch
            {
                
            }

            return result;
        }

        public void DeleteEntertainment(int id)
        {            
            repository.Delete(id);
        }

        public int UpdateEntertainment(EntertainmentSubmitModel entity)
        {
            int result = 0;

            try
            {
                var modifiedEntity = (Entertainment)entity;

                repository.Update(modifiedEntity);

                result = modifiedEntity.Id;
            }
            catch
            {

            }

            return result;
        }

        public object[] GetGendersList()
        {
            return new[] { new { Id = (int)Gender.Both, Name = "Для всех" }, new { Id = (int)Gender.Male, Name = "Для мальчиков" }, new { Id = (int)Gender.Female, Name = "Для девочек" } };
        }
    }
}
