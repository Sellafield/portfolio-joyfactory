﻿using JoyFactory.Models.DataBase;
using JoyFactory.Models.Enums;
using JoyFactory.Models.Operations;
using JoyFactory.Models.ViewModels;
using JoyFactory.Shared.Interfaces;
using Models.SubmitModels;
using Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Domain.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository repository;
        private readonly IOrderStatusRepository orderStatusRepository;
        private readonly IEntertainmentsRepository entertainmentsRepository;
        private readonly IEmployerRepository employersRepository;
        private readonly ICustomerRepository customersRepository;

        public OrderService(IOrderRepository repositoryParam, IEntertainmentsRepository programRepositoryParam, ICustomerRepository customersRepositoryParam, IOrderStatusRepository orderStatusRepositoryParam)
        {
            repository = repositoryParam;
            entertainmentsRepository = programRepositoryParam;
            customersRepository = customersRepositoryParam;
            orderStatusRepository = orderStatusRepositoryParam;
        }

        public DataTableViewModel GetAllOrders()
        {
            var entities = repository.GetAll();
            DataTableViewModel dataTable = ProcessTable(entities);

            return dataTable;
        }

        public DataTableViewModel GetOrdersPage(int rowOffset, int rows, string sortField, int sortOrder)
        {
            PropertyDescriptor prop = TypeDescriptor.GetProperties(typeof(Order)).Find(sortField, true);
            var entities = repository.GetPage(rowOffset, rows + 1, e=>prop.GetValue(e), sortOrder);
            DataTableViewModel dataTable = ProcessTable(entities, rowOffset);

            return dataTable;
        }

        private DataTableViewModel ProcessTable(IEnumerable<Order> entities, int rowOffset = 0)
        {
            DataTableViewModel dataTable = new DataTableViewModel();

            var idColumn = new DataColumnViewModel("Id", "Номер заявки");
            dataTable.Columns.Add(idColumn);

            var statusColumn = new DataColumnViewModel("OrderStatus", "Статус заявки");
            dataTable.Columns.Add(statusColumn);

            var nameColumn = new DataColumnViewModel("OrderName", "Название заявки");
            dataTable.Columns.Add(nameColumn);

            var eventDateColumn = new DataColumnViewModel("EventDate", "Дата проведения");
            dataTable.Columns.Add(eventDateColumn);

            foreach (var entity in entities.OrderBy(e => e.EventDate).Select(e => (OrderListItemViewModel)e).ToList())
            {
                dataTable.Rows.Add(entity);
            }

            var entitiesCount = entities.Count();

            dataTable.TotalRecords = rowOffset + entitiesCount;

            return dataTable;
        }

        public OrderViewModel GetOrder(int id)
        {
            var entity = repository.GetById(id);
            return (OrderViewModel)entity;
        }

        public EntityOperation<OrderViewModel> CreateOrder(OrderSubmitModel entity)
        {
            try
            {
                var newEntity = (Order)entity;

                if (entity.Manager != null)
                {
                    var manager = employersRepository.GetById(entity.Manager.Id);
                    newEntity.Manager = manager;
                }

                //if (entity.CustomerId.GetValueOrDefault(0) > 0)
                //{
                //    var customer = customersRepository.GetById(entity.CustomerId.Value);                    
                //    newEntity.CustomerId = customer.Id;
                //}

                var newTiming = new List<TimingRecord>();

                foreach (var submittedRecord in entity.Timing)
                {
                    var entertainment = entertainmentsRepository.GetById(submittedRecord.EntertainmentId.GetValueOrDefault());
                    newTiming.Add(new TimingRecord { Starts = submittedRecord.Starts, Ends = submittedRecord.Ends, Entertainment = entertainment});
                }

                newEntity.Timing = newTiming;

                repository.Add(newEntity);
                
                return new EntityOperation<OrderViewModel>((OrderViewModel)newEntity);
            }
            catch (Exception ex)
            {
                return new EntityOperation<OrderViewModel>(new Exception("Ошибка при создании заявки", ex));
            }
        }

        public EntityOperation<OrderViewModel> UpdateOrder(OrderSubmitModel entity)
        {
            try
            {
                var modifiedEntity = (Order)entity;
                repository.Update(modifiedEntity);
                return new EntityOperation<OrderViewModel>((OrderViewModel)modifiedEntity);
            }
            catch (Exception ex)
            {
                return new EntityOperation<OrderViewModel>(new Exception("Ошибка при сохранении заявки", ex));
            }
        }

        public OrderSubmitModel GetEditableOrder(int id)
        {
            var entity = repository.GetById(id);
            var editableEntity = (OrderSubmitModel)entity;
            return editableEntity;
        }

        public void DeleteOrder(int id)
        {
            repository.Delete(id);
        }

        public List<OrderStatusViewModel> GetOrderStatuses()
        {
            return orderStatusRepository.GetAll().Select(os=>(OrderStatusViewModel)os).ToList();
        }
    }
}
