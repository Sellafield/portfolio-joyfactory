﻿using JoyFactory.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JoyFactory.Models.SubmitModels;
using JoyFactory.Models.ViewModels;
using JoyFactory.Models.DataBase;

namespace JoyFactory.Domain.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository repository;

        public CustomerService(ICustomerRepository repositoryParam)
        {
            repository = repositoryParam;
        }

        public int CreateCustomer(CustomerSubmitModel entity)
        {
            var newEntity = (Customer)entity;

            repository.Add(newEntity);

            return newEntity.Id;
        }

        public void DeleteCustomer(int id)
        {
            repository.Delete(id);
        }

        public CustomerViewModel GetCustomer(int id)
        {
            return (CustomerViewModel)repository.GetById(id);
        }

        public List<CustomerViewModel> GetCustomerByName(string searchString)
        {
            var entities = repository.GetMany(c => c.FirstName.ToUpper().Contains(searchString.ToUpper())
                || c.MiddleName.ToUpper().Contains(searchString.ToUpper())
                || c.LastName.ToUpper().Contains(searchString.ToUpper()));

            return entities.OrderBy(e => e.LastName).ThenBy(e => e.FirstName).ThenBy(e => e.MiddleName).Select(e => (CustomerViewModel)e).ToList();
        }

        public List<CustomerViewModel> GetCustomers()
        {
            var entities = repository.GetAll();
            return entities.OrderBy(e => e.LastName).ThenBy(e => e.FirstName).ThenBy(e => e.MiddleName).Select(e => (CustomerViewModel)e).ToList();
        }

        public int UpdateCustomer(CustomerSubmitModel entity)
        {
            int result = 0;

            try
            {
                var modifiedEntity = (Customer)entity;

                repository.Update(modifiedEntity);

                result = modifiedEntity.Id;
            }
            catch
            {

            }

            return result;
        }
    }
}
