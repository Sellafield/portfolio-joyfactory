﻿using JoyFactory.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JoyFactory.Models.ViewModels;

namespace JoyFactory.Domain.Services
{
    public class OrderStatusService : IOrderStatusService
    {
        private readonly IOrderStatusRepository repository;        

        public OrderStatusService(IOrderStatusRepository repositoryParam)
        {
            repository = repositoryParam;            
        }

        public List<OrderStatusViewModel> GetStatuses()
        {
            var entities = repository.GetAll();
            return entities.Select(e => (OrderStatusViewModel)e).ToList();
        }
    }
}
