﻿using JoyFactory.Domain.DataAccess;
using JoyFactory.Domain.Services;
using Ninject;
using JoyFactory.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Interfaces;

namespace JoyFactory.Domain
{
    public class ModuleInstaller
    {
        public void Install(IKernel kernel)
        {
            kernel.Bind<IEntertainmentsRepository>().To<EntertainmentsRepository>();
            kernel.Bind<IEntertainmentsService>().To<EntertainmentsService>();

            kernel.Bind<IOrderRepository>().To<OrderRepository>();
            kernel.Bind<IOrderService>().To<OrderService>();

            kernel.Bind<IOrderStatusRepository>().To<OrderStatusRepository>();
            kernel.Bind<IOrderStatusService>().To<OrderStatusService>();

            kernel.Bind<IEmployerRepository>().To<EmployerRepository>();
            kernel.Bind<IEmployerService>().To<EmployerService>();

            kernel.Bind<ICustomerRepository>().To<CustomerRepository>();
            kernel.Bind<ICustomerService>().To<CustomerService>();
        }
    }
}
