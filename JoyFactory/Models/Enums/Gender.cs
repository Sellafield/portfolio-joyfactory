﻿namespace JoyFactory.Models.Enums
{
    public enum Gender
    {
        Both = 0,
        Male = 1,
        Female = 2
    }
}
