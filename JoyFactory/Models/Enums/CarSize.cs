﻿namespace JoyFactory.Domain.Enums
{
    public enum CarSize
    {
        Small = 0,
        Big = 1
    }
}
