﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.Enums
{
    public enum EntertainmentType
    {
        Other = 0,
        Program = 1,
        Addon = 2
    }
}
