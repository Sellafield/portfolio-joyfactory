﻿using JoyFactory.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.ViewModels
{
    public class DataColumnViewModel
    {
        public string Caption { get; set; }
        public string ColumnName { get; set; }
        public DataType DataType { get; set; }

        public DataColumnViewModel(string columnName, string caption)
        {
            ColumnName = columnName;
            Caption = caption;
        }
    }
}
