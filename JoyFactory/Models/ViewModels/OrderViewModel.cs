﻿using JoyFactory.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.ViewModels
{
    public class OrderViewModel
    {
        public long Id { get; set; }

        public DateTime? OrderDate { get; set; }

        public EmployerViewModel Manager { get; set; }

        public OrderStatusViewModel OrderStatus { get; set; }

        public string OrderName { get; set; }

        public DateTime? EventDate { get; set; }

        public CustomerViewModel Customer { get; set; }

        public string HeroADay { get; set; }

        public ICollection<TimingRecordViewModel> Timing { get; set; }

        public static explicit operator OrderViewModel(Order source)
        {
            return new OrderViewModel
            {
                Id = source.Id,
                OrderDate = source.Created.GetValueOrDefault(),
                OrderName = source.OrderName,
                Manager = source.Manager != null ? (EmployerViewModel)source.Manager : null,
                Customer = source.Customer != null ? (CustomerViewModel)source.Customer : null,
                EventDate = source.EventDate.GetValueOrDefault(),
                HeroADay = source.HeroADay,
                Timing = source.Timing != null ? source.Timing.Select(e => (TimingRecordViewModel)e).ToList() : new List<TimingRecordViewModel>(),
                OrderStatus = (OrderStatusViewModel)source.Status
            };
        }
    }
}
