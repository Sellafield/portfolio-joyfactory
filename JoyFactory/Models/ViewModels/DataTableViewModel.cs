﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.ViewModels
{
    public class DataTableViewModel
    {
        public List<DataColumnViewModel> Columns { get; set; }
        public List<dynamic> Rows { get; set; }
        public int TotalRecords { get; set; }

        public DataTableViewModel()
        {
            Columns = new List<DataColumnViewModel>();
            Rows = new List<dynamic>();
        }
    }
}
