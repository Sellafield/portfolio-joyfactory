﻿using JoyFactory.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.ViewModels
{
    public class EmployerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }        

        public static explicit operator EmployerViewModel(Employer source)
        {
            var nameArray = new []{ source.LastName, source.FirstName, source.MiddleName };

            var name = String.Join(" ", nameArray);

            return new EmployerViewModel { Id = source.Id, Name = name };
        }
    }
}
