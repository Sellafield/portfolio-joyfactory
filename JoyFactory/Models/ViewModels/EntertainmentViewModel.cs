﻿using JoyFactory.Models.DataBase;
using JoyFactory.Models.Enums;

namespace JoyFactory.Models.ViewModels
{
    public class EntertainmentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AgeLow { get; set; }
        public int AgeHigh { get; set; }
        public Gender Gender { get; set; }
        public EntertainmentType Type { get; set; }

        public static explicit operator EntertainmentViewModel(Entertainment source)
        {

            return new EntertainmentViewModel
            {
                Id = source.Id,
                Type = source.Type,
                Name = source.Name,
                Description = source.Description,
                AgeLow = source.AgeLow,
                AgeHigh = source.AgeHigh,
                Gender = source.Gender
            };
        }
    }
}
