﻿using JoyFactory.Models.DataBase;
using JoyFactory.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.ViewModels
{
    public class TimingRecordViewModel
    {
        public long Id { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public EntertainmentViewModel Entertainment { get; set; }
        public bool IsMain { get; set; }

        public static explicit operator TimingRecordViewModel (TimingRecord source)
        {
            return new TimingRecordViewModel
            {
                Id = source.Id,
                Starts = source.Starts,
                Ends = source.Ends,
                Entertainment = source.Entertainment != null ? (EntertainmentViewModel)source.Entertainment : null,
                IsMain = source.IsMain.GetValueOrDefault(false)
            };
        }
    }
}
