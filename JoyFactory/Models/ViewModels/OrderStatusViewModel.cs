﻿using JoyFactory.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.ViewModels
{
    public class OrderStatusViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public static explicit operator OrderStatusViewModel(OrderStatus source)
        {
            return new OrderStatusViewModel { Id = source.Id, Name = source.Name };
        }
    }
}
