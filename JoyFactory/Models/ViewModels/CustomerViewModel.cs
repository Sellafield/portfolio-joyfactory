﻿using JoyFactory.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.ViewModels
{
    public class CustomerViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string FullName { get { return String.Format("{0} {1} {2}", LastName, FirstName, MiddleName); } }

        public static explicit operator CustomerViewModel(Customer source)
        {
            return new CustomerViewModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                MiddleName = source.MiddleName
            };
        }
    }
}
