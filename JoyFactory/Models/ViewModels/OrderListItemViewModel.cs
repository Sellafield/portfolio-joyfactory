﻿using JoyFactory.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.ViewModels
{
    public class OrderListItemViewModel
    {
        public long Id { get; set; }

        public OrderStatusViewModel OrderStatus { get; set; }

        public string OrderName { get; set; }

        public string OrderDate { get; set; }

        public EmployerViewModel Manager { get; set; }

        public string EventDate { get; set; }

        public CustomerViewModel Customer { get; set; }

        public string HeroADay { get; set; }

        public static explicit operator OrderListItemViewModel(Order source)
        {
            return new OrderListItemViewModel
            {
                Id = source.Id,
                OrderStatus = (OrderStatusViewModel)source.Status,
                OrderName = source.OrderName,
                OrderDate = source.Created.GetValueOrDefault().ToString("d"),
                Manager = source.Manager != null ? (EmployerViewModel)source.Manager : null,
                Customer = source.Customer != null ? (CustomerViewModel)source.Customer : null,
                EventDate = source.EventDate.GetValueOrDefault().ToString("d"),
                HeroADay = source.HeroADay
            };
        }
    }
}
