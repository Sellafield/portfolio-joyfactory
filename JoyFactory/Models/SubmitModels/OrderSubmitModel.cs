﻿using JoyFactory.Models.DataBase;
using JoyFactory.Models.SubmitModels;
using JoyFactory.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.SubmitModels
{
    public class OrderSubmitModel
    {
        public int Id { get; set; }

        public DateTime? OrderDate { get; set; }

        public EmployerViewModel Manager { get; set; }

        public string OrderName { get; set; }

        public DateTime? EventDate { get; set; }

        public string HeroADay { get; set; }

        public List<TimingRecordSubmitModel> Timing { get; set; } = new List<TimingRecordSubmitModel>();

        public int? CustomerId { get; set; }

        public OrderStatusViewModel OrderStatus { get; set; }

        public static explicit operator OrderSubmitModel(Order source)
        {
            return new OrderSubmitModel
            {
                Id = source.Id,
                OrderName = source.OrderName,
                OrderDate = source.Created.GetValueOrDefault(),
                Manager = source.Manager != null ? (EmployerViewModel)source.Manager : null,
                EventDate = source.EventDate,
                HeroADay = source.HeroADay,
                Timing = source.Timing.Select(t => (TimingRecordSubmitModel)t).ToList(),
                CustomerId = source.CustomerId,
                OrderStatus = (OrderStatusViewModel)source.Status
            };
        }
    }    
}
