﻿
using JoyFactory.Models.DataBase;
using JoyFactory.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.SubmitModels
{
    public class TimingRecordSubmitModel
    {
        public int Id { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public int? EntertainmentId { get; set; }
        public List<EntertainmentViewModel> EntertainmentsSelectList { get; set; }

        public static explicit operator TimingRecordSubmitModel(TimingRecord source)
        {
            return new TimingRecordSubmitModel
            {
                Id = source.Id,
                Starts = source.Starts,
                Ends = source.Ends,
                EntertainmentId = source.EntertainmentId
            };
        }
    }
}
