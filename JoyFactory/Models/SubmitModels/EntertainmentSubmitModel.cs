﻿using JoyFactory.Models.Enums;

namespace JoyFactory.Models.SubmitModels
{
    public class EntertainmentSubmitModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AgeLow { get; set; }
        public int AgeHigh { get; set; }
        public Gender Gender { get; set; }
        public EntertainmentType Type { get; set; }
    }
}
