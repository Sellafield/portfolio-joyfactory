﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.DataBase
{
    public class Actor : Employer
    {
        public List<ActorRole> ActorRoles { get; set; }
    }
}
