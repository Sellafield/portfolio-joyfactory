﻿using JoyFactory.Models.Enums;
using JoyFactory.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.SubmitModels;
using JoyFactory.Models.SubmitModels;

namespace JoyFactory.Models.DataBase
{
    public class Entertainment
    {  
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public EntertainmentType Type { get; set; }      
        public int AgeLow { get; set; }
        public int AgeHigh { get; set; }
        public Gender Gender { get; set; }
        public List<ActorRole> ActorRoles { get; set; }

        public static explicit operator Entertainment(EntertainmentViewModel source)
        {
            return new Entertainment { Id = source.Id, Name = source.Name, Description = source.Description };
        }

        public static explicit operator Entertainment(EntertainmentSubmitModel source)
        {
            return new Entertainment { Id = source.Id, Name = source.Name, Description = source.Description, Type = source.Type, AgeLow = source.AgeLow, AgeHigh = source.AgeHigh, Gender = source.Gender };
        }
    }
}
