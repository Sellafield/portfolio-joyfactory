﻿using Models.SubmitModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.DataBase
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime? Created { get; set; }
        public string Creator { get; set; }

        public int? ManagerId { get; set; }
        public Employer Manager { get; set; }

        public string OrderName { get; set; }

        public DateTime? EventDate { get; set; }

        public int? StatusId { get; set; }
        public virtual OrderStatus Status { get; set; }

        public int? CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public string HeroADay { get; set; }
        public int HeroADayAge { get; set; }        

        public virtual ICollection<TimingRecord> Timing { get; set; }

        public Order()
        {            
            Timing = new List<TimingRecord>();
        }

        public static explicit operator Order(OrderSubmitModel source)
        {
            return new Order {
                Id = source.Id,
                Created = source.OrderDate,
                OrderName = source.OrderName,
                EventDate = source.EventDate,
                HeroADay = source.HeroADay,
                CustomerId = source.CustomerId,
                Status = (OrderStatus)source.OrderStatus,
                StatusId = source.OrderStatus.Id
            };
        }
    }
}
