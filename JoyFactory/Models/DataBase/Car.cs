﻿using JoyFactory.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.DataBase
{
    public class Car
    {
        public string Model { get; set; }
        public string Color { get; set; }
        public string Number { get; set; }
        public CarSize CarSize { get; set; }
    }
}
