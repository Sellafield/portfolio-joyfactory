﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.DataBase
{
    public class SystemRole
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
    }
}
