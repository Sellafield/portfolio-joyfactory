﻿using JoyFactory.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.DataBase
{
    public class OrderStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsInitial { get; set; }
        public bool? IsFinal { get; set; }

        public static explicit operator OrderStatus(OrderStatusViewModel source)
        {
            return new OrderStatus { Id = source.Id, Name = source.Name };
        }
    }
}
