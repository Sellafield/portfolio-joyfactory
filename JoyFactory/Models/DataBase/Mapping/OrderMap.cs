﻿using JoyFactory.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.DataBase.Mapping
{
    public class OrderMap : EntityTypeConfiguration<Order>
    {
        public OrderMap()
        {
            ToTable("Order");

            HasKey(e => e.Id);

            Property(e => e.Created).HasColumnType("datetime");
            Property(e => e.Creator).HasMaxLength(200).IsUnicode(true);
            Property(e => e.Manager).HasMaxLength(200).IsUnicode(true);
            Property(e => e.EventDate).HasColumnType("datetime");
            Property(e => e.Customer).HasMaxLength(200).IsUnicode(true);
            Property(e => e.HeroADay).HasMaxLength(200).IsUnicode(true);
            Property(e => e.HeroADayAge);

            HasMany(e => e.Programs).WithRequired(oe => oe.Order).HasForeignKey(oe => oe.OrderId);
        }
    }
}
