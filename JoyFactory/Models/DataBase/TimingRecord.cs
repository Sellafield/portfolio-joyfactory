﻿using JoyFactory.Models.DataBase;
using JoyFactory.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.DataBase
{
    public class TimingRecord
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public EntertainmentType EntertainmentType { get; set; }
        public bool? IsMain { get; set; }

        public int? EntertainmentId { get; set; }
        public virtual Entertainment Entertainment { get; set; }

        public virtual Order Order { get; set; }
    }
}
