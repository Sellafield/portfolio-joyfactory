﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.Operations
{
    public class Operation
    {
        public bool Success { get; private set; }

        public Exception Exception { get; private set; }

        public Operation()
        {
            Success = true;
        }

        public Operation(Exception exception)
        {
            Success = false;
            Exception = exception;
        }
    }
}
