﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Models.Operations
{
    public class EntityOperation<T> : Operation where T:class
    {
        public T Entity { get; set; }

        public EntityOperation(T entity) : base()
        {
            Entity = entity;
        }

        public EntityOperation(Exception exception) : base(exception) { }
    }
}
