import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ButtonModule, CalendarModule, DataTableModule, DropdownModule, InputTextModule, MessagesModule, SharedModule } from 'primeng/primeng';

import { ElementsModule } from '../shared/elements/elements.module';
import { OrdersRoutingModule } from './orders-routing.module';

import { OrdersListComponent } from './orders-list.component';
import { OrdersFormComponent } from './orders-form.component';

@NgModule({
    imports: [ButtonModule, CalendarModule, CommonModule, DataTableModule, DropdownModule, InputTextModule, MessagesModule, SharedModule, ElementsModule, OrdersRoutingModule, FormsModule],
    declarations: [ OrdersListComponent, OrdersFormComponent ],
    exports: [ OrdersListComponent, OrdersFormComponent ]
})

export class OrdersModule {}