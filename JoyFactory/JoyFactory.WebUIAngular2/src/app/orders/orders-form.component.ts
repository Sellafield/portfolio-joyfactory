import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { OrdersService } from '../shared/services/orders.service';
import { OrderStatusesService } from '../shared/services/order-statuses.service';

import { Order } from '../shared/models/order.model';
import { OrderStatus } from '../shared/models/order-status.model';

import { Message, SelectItem } from 'primeng/primeng';

@Component({
    selector: 'orders-form',
    templateUrl: './orders-form.component.html',
    styleUrls: [ './orders-form.component.css' ]
})

export class OrdersFormComponent {
    @Input() order: Order = new Order();

    @Input() messages: Message[] = [];

    orderStatuses: SelectItem[] = [];

    constructor(private route: ActivatedRoute, private router: Router, private ordersService: OrdersService, private orderStatusesService: OrderStatusesService) { }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            let id = +params['id']; // (+) converts string 'id' to a number
            if (id > 0)
                this.ordersService.getOrder(id).then(order => this.ProcessOrder(order));
            else
                this.ProcessOrder(new Order());
        });

        this.orderStatusesService.getOrderStatuses().then(orderStatuses => this.ProcessOrderStatuses(orderStatuses));
    }

    private ProcessOrder(order: Order, message?: string) {        
        this.order = order;

        if (message)
        {
            this.messages = [];
            this.messages.push({ severity: 'success', detail: <string>message })
        }
    }

    private ProcessOrderStatuses(orderStatuses: OrderStatus[]) {
        for (let orderStatus of orderStatuses) {
            this.orderStatuses.push({ label: orderStatus.Name, value: orderStatus.Id });
        }   
    }

    Save() {
        this.ordersService.saveOrder(this.order).then(order => this.ProcessOrder(order, "������ ���������"), error => this.messages.push({ severity: 'error', summary: 'Error Message', detail: <string>error }));
    }

    Return() {
        this.router.navigate(['/orders/list']);
    }
}