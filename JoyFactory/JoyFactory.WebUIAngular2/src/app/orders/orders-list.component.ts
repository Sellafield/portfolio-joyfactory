import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { LazyLoadEvent } from 'primeng/primeng';

import { OrdersService } from '../shared/services/orders.service';

import { Order } from '../shared/models/order.model';
import { DataTable } from '../shared/models/data-table.model';

@Component({
    selector: 'orders-list',
    templateUrl: './orders-list.component.html',
    styleUrls: [ './orders-list.component.css' ]
})

export class OrdersListComponent {    
    private ordersList: DataTable = new DataTable();

    constructor(private router: Router, private ordersService: OrdersService) { }      

    //ngOnInit() {
    //    this.LoadOrders();
    //}

    //private LoadOrders(): void {
    //    this.ordersService.getTable().then(orders=> this.ProcessOrders(orders));
    //}

    private LoadOrdersLazy(event: LazyLoadEvent): void {
        this.ordersService.getTable(event).then(orders => this.ProcessOrders(orders));
    }

    private ProcessOrders(orders: DataTable) {
        console.log(orders);
        this.ordersList = orders;
    }

    orderSelected(sender: any) {
        let order = sender.data;
        this.router.navigate(['/orders/view', order.Id]);
    }

    CreateOrder() {
        this.router.navigate(['/orders/view', 0])
    }
}