import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';

import { OrdersListComponent }    from './orders-list.component';
import { OrdersFormComponent } from './orders-form.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'orders/list',  component: OrdersListComponent },
      { path: 'orders/view/:id', component: OrdersFormComponent },      
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class OrdersRoutingModule { }
