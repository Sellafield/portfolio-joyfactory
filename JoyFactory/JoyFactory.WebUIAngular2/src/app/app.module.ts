import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { TabMenuModule } from 'primeng/primeng';

import { OrdersService } from './shared/services/orders.service';
import { OrderStatusesService } from './shared/services/order-statuses.service';

import { AppComponent } from './app.component';

import { OrdersModule } from './orders/orders.module';

@NgModule({
    imports: [ BrowserModule, FormsModule, HttpModule, AppRoutingModule, TabMenuModule, OrdersModule ],
    declarations: [AppComponent],
    providers: [OrdersService, OrderStatusesService],
    bootstrap: [ AppComponent ]
})

export class AppModule { }