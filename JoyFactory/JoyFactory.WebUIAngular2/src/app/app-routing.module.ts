import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OrdersListComponent } from './orders/orders-list.component';

@NgModule({
    imports: [RouterModule.forRoot([
        {path: '', redirectTo: 'orders/list', pathMatch: 'full'},
        {path: 'orders/list', component: OrdersListComponent },
    ])],
    exports: [ RouterModule ]
})

export class AppRoutingModule { } 