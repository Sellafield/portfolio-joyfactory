import { Component, Input, Output, EventEmitter } from '@angular/core';

import { DataTable } from '../../models/data-table.model';
import { DataRow } from '../../models/data-row.model';

@Component({
    selector: 'commonDataTable',
    templateUrl: './datatable.component.html',
    styleUrls: ['./datatable.component.css']
})

export class DataTableComponent { 
    @Input() dataTable: DataTable;

    @Output() onRowClick = new EventEmitter<DataRow>();

    rowClicked(row: any) {
        this.onRowClick.emit(row);
    }
}