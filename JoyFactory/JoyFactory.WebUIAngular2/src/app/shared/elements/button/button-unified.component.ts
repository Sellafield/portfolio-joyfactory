import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'buttonUnified',
    templateUrl: './button-unified.component.html',
    styleUrls: [ './button-unified.component.css' ]
})

export class ButtonUnifiedComponent {
    @Input() buttonType: String = 'button';
    @Input() buttonText: String;
    @Input() buttonImageUrl: String;
    @Input() buttonCssClass: String;

    @Output() buttonClick = new EventEmitter();

    onButtonClicked() {
        this.buttonClick.emit();
    }
}