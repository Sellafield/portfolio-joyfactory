import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataTableComponent } from './datatable/datatable.component';
import { ButtonUnifiedComponent } from './button/button-unified.component';

@NgModule({
    imports: [ CommonModule ],
    declarations: [ DataTableComponent, ButtonUnifiedComponent ],
    exports: [ DataTableComponent, ButtonUnifiedComponent ]
})

export class ElementsModule {}