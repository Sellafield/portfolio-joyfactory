import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { LazyLoadEvent } from 'primeng/primeng';

import { DataTable } from '../models/data-table.model';
import { DataColumn } from '../models/data-column.model';

import { DataType } from '../models/data-type.enum';
import { Order } from '../models/order.model';
import { OrderStatus } from '../models/order-status.model';

@Injectable()
export class OrdersService {
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }    

    getTable(event: LazyLoadEvent): Promise<DataTable> {
        let paramsString = '?rowOffset=' + event.first + '&rows=' + event.rows + (event.sortField ? "&sortField=" + event.sortField : "") + "&sortOrder=" + event.sortOrder;
        return this.http.get('api/orders/' + paramsString).toPromise().then(response => Promise.resolve(response.json().Data as DataTable)).catch(this.handleError);
    }

    getOrder(Id: number): Promise<Order> {
        let paramsString = '?id=' + Id;
        return this.http.get('api/orders/' + paramsString).toPromise().then(response => Promise.resolve(this.ConvertToOrder(response.json().Data))).catch(this.handleError);
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json().Data || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Promise.reject(errMsg);
    }

    private ConvertToOrder(source: any) : Order {
        var result = new Order(source.Id, source.OrderName, source.EventDate, new OrderStatus(source.OrderStatus.Id, source.OrderStatus.Name));
        
        return result;
    }

    saveOrder(order: Order): Promise<any> {        
        const url = order.Id > 0 ? 'api/orders/'+order.Id : 'api/orders';
        return this.http
            .post(url, JSON.stringify(order), { headers: this.headers })
            .toPromise().then(response => Promise.resolve(this.ProcessResponse(response.json().Data)))
            .catch(this.handleError);
    }

    private ProcessResponse(response: any): Promise<any>
    {
        if (response.Success == false) {
            return this.handleError(response.Exception.Message);
        } else
        {
            return Promise.resolve(this.ConvertToOrder(response.Entity));
        }
    }
}