﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { OrderStatus } from '../models/order-status.model';

@Injectable()
export class OrderStatusesService {
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    getOrderStatuses(): Promise<OrderStatus[]> {        
        return this.http.get('api/orderstatuses').toPromise().then(response => Promise.resolve(this.ConvertToOrderStatusArray(response.json().Data))).catch(this.handleError);
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json().Data || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Promise.reject(errMsg);
    }

    private ConvertToOrderStatusArray(source: any): OrderStatus[] {
        let orderStatuses: OrderStatus[]  = [];

        for (let orderStatusSource of source) {
            let orderStatus = new OrderStatus(orderStatusSource.Id, orderStatusSource.Name);
            orderStatuses.push(orderStatus);
        }

        return orderStatuses;
    }    
}