export enum DataType {
    Number = 1,
    String = 2,
    DateTime = 3,
}