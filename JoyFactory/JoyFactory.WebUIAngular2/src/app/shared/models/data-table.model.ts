import { DataColumn } from './data-column.model';
import { DataRow } from './data-row.model';

export class DataTable{
    Columns: DataColumn[];
    Rows: DataRow[];
    TotalRecords: number;
}