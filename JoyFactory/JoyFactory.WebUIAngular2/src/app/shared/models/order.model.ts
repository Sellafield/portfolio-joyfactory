import { OrderStatus } from './order-status.model';

export class Order {
    public Id: number;
    public OrderStatus: OrderStatus;
    public OrderName: string;
    public EventDate: Date;

    constructor(id?: number, orderName?: string, eventDate?: string, status?: OrderStatus) { 
        this.Id = id;
        this.OrderName = orderName;
        this.EventDate = eventDate ? new Date(eventDate) : new Date();
        this.OrderStatus = status ? status : new OrderStatus();
    }    
}