import { DataType } from './data-type.enum';

export class DataColumn {
    constructor(public Caption: string, public ColumnName: string, public DataType: DataType) { }

    GetFormattedValue(value: any) : any {
        switch (this.DataType) {
            case DataType.Number: {
                return value as number;
            }
            case DataType.String: {
                return value as string;
            }
            case DataType.DateTime: {
                return (value as Date).toLocaleDateString();
            }
        }
    }
}