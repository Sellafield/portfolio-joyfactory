import { Component, OnInit } from '@angular/core';

import { MenuItem } from 'primeng/primeng';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: [ './app.component.css' ]
})

export class AppComponent {
    private items: MenuItem[];

    ngOnInit() {
        this.items = [
            {
                label: 'Заказы',
                routerLink: '/orders/list'
            }
        ]
    }
}