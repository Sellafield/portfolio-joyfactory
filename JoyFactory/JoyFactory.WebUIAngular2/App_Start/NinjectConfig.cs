﻿using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JoyFactory.WebUIAngular2.App_Start
{
    public class NinjectConfig
    {
        private static Lazy<IKernel> kernel = new Lazy<IKernel>(() =>
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        });

        public static IKernel GetConfiguredKernel()
        {
            return kernel.Value;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        public static void RegisterServices(IKernel kernel)
        {
            new Storage.ModuleInstaller().Install(kernel);
            new Domain.ModuleInstaller().Install(kernel);
        }
    }
}