﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace JoyFactory.WebUIAngular2
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}/{resource}",
                defaults: new { resource = RouteParameter.Optional, id = RouteParameter.Optional }
            );

            //Стандартный xml-сериализатор не поддерживает динамические типы, поэтому необходимо помочь
            //var xmlFormatter = config.Formatters.XmlFormatter;
            //xmlFormatter.SetSerializer<AssetListView>(new AssetListViewSerializer());
            //xmlFormatter.SetSerializer<CategoryListView>(new CategoryListViewSerializer());
            //xmlFormatter.SetSerializer<ExecutorGroupListView>(new ExecutorGroupListViewSerializer());
            //xmlFormatter.SetSerializer<ServiceListView>(new ServiceListViewXmlSerializer());
            //xmlFormatter.SetSerializer<TaskFormView>(new TaskFormViewXmlSerializer());
            //xmlFormatter.SetSerializer<TaskLifetimeListView>(new TaskLifetimeListViewXmlSerializer());
            //xmlFormatter.SetSerializer<TaskListView>(new TaskListViewXmlSerializer());
            //xmlFormatter.SetSerializer<TaskTypeListView>(new TaskTypeListViewSerializer());
            //xmlFormatter.SetSerializer<UserListView>(new UserListViewSerializer());
            //xmlFormatter.SetSerializer<ExpensesListView>(new ExpensesListViewSerializer());
            //xmlFormatter.SetSerializer<ExpenseView>(new ExpenseViewSerializer());
            //xmlFormatter.SetSerializer<CompanyListView>(new CompanyListViewSerializer());
            //xmlFormatter.SetSerializer<AssetTypeListView>(new AssetTypeListViewSerializer());
            //xmlFormatter.SetSerializer<AssetView>(new CommonViewSerializer<AssetView>());
            //xmlFormatter.SetSerializer<CompanyView>(new CommonViewSerializer<CompanyView>());
        }
    }
}