﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;

namespace JoyFactory.WebUIAngular2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            var config = GlobalConfiguration.Configuration;

            WebApiConfig.Register(config);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
