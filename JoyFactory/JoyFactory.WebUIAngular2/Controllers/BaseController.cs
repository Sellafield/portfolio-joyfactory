﻿using JoyFactory.WebUIAngular2.App_Start;
using Ninject;
using System.Web.Http;

namespace JoyFactory.WebUIAngular2.Controllers
{
    public class BaseController : ApiController
    {
        public IKernel kernel
        {
            get { return NinjectConfig.GetConfiguredKernel(); }
        }
    }
}