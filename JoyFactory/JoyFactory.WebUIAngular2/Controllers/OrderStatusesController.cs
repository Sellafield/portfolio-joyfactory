﻿using JoyFactory.Shared.Interfaces;
using Models.SubmitModels;
using System;
using System.Web.Mvc;

namespace JoyFactory.WebUIAngular2.Controllers
{
    public class OrderStatusesController : BaseController
    {
        [HttpGet]
        public JsonNetResult GetOrderStatuses()
        {
            var service = (IOrderService)(kernel.GetService(typeof(IOrderService)));

            //return Json(service.GetAllOrders(), JsonRequestBehavior.AllowGet);
            return new JsonNetResult(service.GetOrderStatuses(), JsonRequestBehavior.AllowGet);
        }        
    }
}