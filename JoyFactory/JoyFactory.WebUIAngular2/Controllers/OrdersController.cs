﻿using JoyFactory.Shared.Interfaces;
using Models.SubmitModels;
using System;
using System.Web.Mvc;

namespace JoyFactory.WebUIAngular2.Controllers
{
    public class OrdersController : BaseController
    {
        [HttpGet]
        public JsonNetResult GetOrders(int rowOffset = 0, int rows = 20, string sortField = "EventDate", int sortOrder = -1)
        {
            var service = (IOrderService)(kernel.GetService(typeof(IOrderService)));

            //return Json(service.GetAllOrders(), JsonRequestBehavior.AllowGet);
            return new JsonNetResult(service.GetOrdersPage(rowOffset, rows, sortField, sortOrder), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonNetResult GetOrder(int id)
        {
            var service = (IOrderService)(kernel.GetService(typeof(IOrderService)));
            
            return new JsonNetResult(service.GetOrder(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonNetResult UpdateOrder(int id, OrderSubmitModel entity)
        {
            var service = (IOrderService)(kernel.GetService(typeof(IOrderService)));

            return new JsonNetResult(service.UpdateOrder(entity), JsonRequestBehavior.AllowGet);            
        }

        [HttpPost]
        public JsonNetResult CreateOrder(OrderSubmitModel entity)
        {
            var service = (IOrderService)(kernel.GetService(typeof(IOrderService)));

            return new JsonNetResult(service.CreateOrder(entity), JsonRequestBehavior.AllowGet);
        }
    }
}