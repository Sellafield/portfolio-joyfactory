﻿using JoyFactory.Storage;
using JoyFactory.Storage.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Storage
{
    public class JoyFactoryContextFactory : IJoyFactoryContextFactory
    {
        private JoyFactoryContext dataContext;

        public JoyFactoryContextFactory()
        {
            Database.SetInitializer<JoyFactoryContext>(null);
        }

        public JoyFactoryContext Get()
        {
            return dataContext ?? (dataContext = new JoyFactoryContext());
        }

        public void Dispose()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }
    }
}
