﻿using JoyFactory.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Storage
{
    public partial class JoyFactoryContext : DbContext
    {
        static JoyFactoryContext()
        {
            Database.SetInitializer<JoyFactoryContext>(null);
        }

        public JoyFactoryContext() : base("Name=JoyFactoryContext")
        {

        }

        public DbSet<Entertainment> Entertainments { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<TimingRecord> Timing { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Employer> Employers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderStatus>().HasKey(os => os.Id);
            modelBuilder.Entity<Order>().HasKey(o => o.Id);
            modelBuilder.Entity<Order>().HasMany(t=>t.Timing).WithOptional(t=>t.Order).WillCascadeOnDelete(true);
            modelBuilder.Entity<Order>().HasRequired(o => o.Status).WithMany().HasForeignKey(o => o.StatusId).WillCascadeOnDelete(true);
            modelBuilder.Entity<Customer>().HasMany(c => c.Orders).WithOptional(o => o.Customer).HasForeignKey(o=>o.CustomerId).WillCascadeOnDelete(true);
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }
    }
}
