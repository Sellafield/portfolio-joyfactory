﻿using JoyFactory.Storage;
using JoyFactory.Storage.Interfaces;
using Ninject;

namespace JoyFactory.Storage
{
    public class ModuleInstaller
    {
        public void Install(IKernel kernel)
        {
            kernel.Bind<IJoyFactoryContextFactory>().To<JoyFactoryContextFactory>();
        }
    }
}
