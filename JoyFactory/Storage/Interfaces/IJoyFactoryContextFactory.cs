﻿
using JoyFactory.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Storage.Interfaces
{
    public interface IJoyFactoryContextFactory
    {
        JoyFactoryContext Get();
    }
}
