namespace Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderOrderStatus : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "StatusId", "dbo.OrderStatus");
            DropIndex("dbo.Orders", new[] { "StatusId" });
            AlterColumn("dbo.Orders", "StatusId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "StatusId");
            AddForeignKey("dbo.Orders", "StatusId", "dbo.OrderStatus", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "StatusId", "dbo.OrderStatus");
            DropIndex("dbo.Orders", new[] { "StatusId" });
            AlterColumn("dbo.Orders", "StatusId", c => c.Int());
            CreateIndex("dbo.Orders", "StatusId");
            AddForeignKey("dbo.Orders", "StatusId", "dbo.OrderStatus", "Id");
        }
    }
}
