namespace Storage.Migrations
{
    using JoyFactory.Models.DataBase;
    using JoyFactory.Models.Enums;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<JoyFactory.Storage.JoyFactoryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private string GenerateOrderName() {
            var variants = new string[] { "��������� ������", "�����-���", "������� �������", "����� ���", "��������� ������" };

            var index = new Random().Next(0, 5);

            return variants[index];
        }

        private int GenerateOrderStatus(int[] possibleStatusIds)
        {
            var index = new Random().Next(0, possibleStatusIds.Length);

            return possibleStatusIds[index];
        }

        protected override void Seed(JoyFactory.Storage.JoyFactoryContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.OrderStatuses.AddOrUpdate(
                e => e.Name,
                new OrderStatus { Name = "������", IsInitial = true },
                new OrderStatus { Name = "� ������" },
                new OrderStatus { Name = "��������", IsFinal = true },
                new OrderStatus { Name = "�������", IsFinal = false }
            );

            var orderStatusIds = context.OrderStatuses.Select(os => os.Id).ToArray();

            var oldOrders = context.Orders;
            context.Orders.RemoveRange(oldOrders);
            var now = DateTime.Now.AddYears(-1);
            for (var i = 1; i<=200; i++)
            {
                context.Orders.Add(new Order { OrderName = GenerateOrderName(), EventDate = now, StatusId = GenerateOrderStatus(orderStatusIds) });
                var daysDifference = new Random().Next(1, 8);
                now = now.AddDays(daysDifference);
            }            


            context.Entertainments.AddOrUpdate(
                e => e.Name,
                new Entertainment { Name = "������� �������", Description = "������� ������� �������� ��� ������ ������� ����� ��� � ������", AgeLow = 1, AgeHigh = 3, Gender = Gender.Both, Type = EntertainmentType.Program },
                new Entertainment { Name = "�����-��� � ���-���-���", Description = "��������� ��������� � �������� ������ ����������� ���������� �����������", AgeLow = 2, AgeHigh = 5, Gender = Gender.Male, Type = EntertainmentType.Program },
                new Entertainment { Name = "������� ������", Description = "�������� � �������� ����� ���� ������� ������", AgeLow = 4, AgeHigh = 5, Gender = Gender.Female, Type = EntertainmentType.Program }
            );

            

            context.Employers.AddOrUpdate(e => e.LastName,
                new Employer { LastName = "������", FirstName = "����", MiddleName = "����������"});
        }
    }
}
