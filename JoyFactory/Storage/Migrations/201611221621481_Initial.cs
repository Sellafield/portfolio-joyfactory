namespace Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastName = c.String(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                        UserId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        Password = c.String(),
                        AllowLogin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SystemRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Alias = c.String(),
                        Name = c.String(),
                        User_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.ActorRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Actor_Id = c.Int(),
                        Entertainment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employers", t => t.Actor_Id)
                .ForeignKey("dbo.Entertainments", t => t.Entertainment_Id)
                .Index(t => t.Actor_Id)
                .Index(t => t.Entertainment_Id);
            
            CreateTable(
                "dbo.Entertainments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Type = c.Int(nullable: false),
                        AgeLow = c.Int(nullable: false),
                        AgeHigh = c.Int(nullable: false),
                        Gender = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Created = c.DateTime(),
                        Creator = c.String(),
                        ManagerId = c.Int(),
                        EventDate = c.DateTime(),
                        StatusId = c.Int(),
                        CustomerId = c.Int(),
                        HeroADay = c.String(),
                        HeroADayAge = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.Employers", t => t.ManagerId)
                .ForeignKey("dbo.OrderStatus", t => t.StatusId)
                .Index(t => t.ManagerId)
                .Index(t => t.StatusId)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastName = c.String(),
                        FirstName = c.String(),
                        MiddleName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsInitial = c.Boolean(),
                        IsFinal = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TimingRecords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(),
                        Starts = c.DateTime(),
                        Ends = c.DateTime(),
                        EntertainmentType = c.Int(nullable: false),
                        IsMain = c.Boolean(),
                        EntertainmentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entertainments", t => t.EntertainmentId)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.OrderId)
                .Index(t => t.EntertainmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TimingRecords", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.TimingRecords", "EntertainmentId", "dbo.Entertainments");
            DropForeignKey("dbo.Orders", "StatusId", "dbo.OrderStatus");
            DropForeignKey("dbo.Orders", "ManagerId", "dbo.Employers");
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.ActorRoles", "Entertainment_Id", "dbo.Entertainments");
            DropForeignKey("dbo.ActorRoles", "Actor_Id", "dbo.Employers");
            DropForeignKey("dbo.Employers", "UserId", "dbo.Users");
            DropForeignKey("dbo.SystemRoles", "User_Id", "dbo.Users");
            DropIndex("dbo.TimingRecords", new[] { "EntertainmentId" });
            DropIndex("dbo.TimingRecords", new[] { "OrderId" });
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            DropIndex("dbo.Orders", new[] { "StatusId" });
            DropIndex("dbo.Orders", new[] { "ManagerId" });
            DropIndex("dbo.ActorRoles", new[] { "Entertainment_Id" });
            DropIndex("dbo.ActorRoles", new[] { "Actor_Id" });
            DropIndex("dbo.SystemRoles", new[] { "User_Id" });
            DropIndex("dbo.Employers", new[] { "UserId" });
            DropTable("dbo.TimingRecords");
            DropTable("dbo.OrderStatus");
            DropTable("dbo.Customers");
            DropTable("dbo.Orders");
            DropTable("dbo.Entertainments");
            DropTable("dbo.ActorRoles");
            DropTable("dbo.SystemRoles");
            DropTable("dbo.Users");
            DropTable("dbo.Employers");
        }
    }
}
