namespace Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "OrderName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "OrderName");
        }
    }
}
