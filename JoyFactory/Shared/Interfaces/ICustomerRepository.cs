﻿using JoyFactory.Models.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Shared.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
