﻿using JoyFactory.Models.Operations;
using JoyFactory.Models.ViewModels;
using Models.SubmitModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Shared.Interfaces
{
    public interface IOrderService
    {
        DataTableViewModel GetAllOrders();

        DataTableViewModel GetOrdersPage(int rowOffset, int rows, string sortField, int sortOrder);

        EntityOperation<OrderViewModel> CreateOrder(OrderSubmitModel entity);

        OrderViewModel GetOrder(int id);

        EntityOperation<OrderViewModel> UpdateOrder(OrderSubmitModel entity);

        OrderSubmitModel GetEditableOrder(int id);

        List<OrderStatusViewModel> GetOrderStatuses();

        void DeleteOrder(int Id);
    }
}
