﻿using JoyFactory.Models.SubmitModels;
using JoyFactory.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Shared.Interfaces
{
    public interface ICustomerService
    {
        List<CustomerViewModel> GetCustomers();        
        
        CustomerViewModel GetCustomer(int id);
        List<CustomerViewModel> GetCustomerByName(string searchString);       
        int CreateCustomer(CustomerSubmitModel entity);
        void DeleteCustomer(int id);
        int UpdateCustomer(CustomerSubmitModel entity);
    }
}
