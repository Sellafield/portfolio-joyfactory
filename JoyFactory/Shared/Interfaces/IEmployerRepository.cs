﻿using JoyFactory.Models.DataBase;
using Shared.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoyFactory.Shared.Interfaces
{
    public interface IEmployerRepository : IReadOnlyRepository<Employer>
    {
    }
}
