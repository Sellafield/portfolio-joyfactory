﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Interfaces.Base
{
    public interface IReadOnlyRepository<T> where T : class
    {
        T GetById(int id);
        T GetByCondition(Func<T, bool> condition);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetPage<TKey>(int rowOffset, int rows, Func<T, TKey> sortSelector, int sortOrder);
        IEnumerable<T> GetMany(Func<T, bool> condition);
    }
}
