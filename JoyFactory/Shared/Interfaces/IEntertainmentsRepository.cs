﻿using JoyFactory.Models.DataBase;
using JoyFactory.Shared.Interfaces;
using Shared.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Interfaces
{
    public interface IEntertainmentsRepository : IRepository<Entertainment>
    {
    }
}
