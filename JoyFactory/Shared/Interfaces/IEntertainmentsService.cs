﻿using JoyFactory.Models.DataBase;
using JoyFactory.Models.Enums;
using JoyFactory.Models.SubmitModels;
using JoyFactory.Models.ViewModels;
using Models.SubmitModels;
using System;
using System.Collections.Generic;

namespace Shared.Interfaces
{
    public interface IEntertainmentsService
    {
        List<EntertainmentViewModel> GetEntertainments();
        List<EntertainmentViewModel> GetEntertainments(EntertainmentType type);
        object[] GetGendersList();

        List<EntertainmentViewModel> GetEntertainmentsBy(Func<int, int, Gender, bool> condition);
        EntertainmentViewModel GetEntertainment(int id);
        int CreateEntertainment(EntertainmentSubmitModel entity);        
        void DeleteEntertainment(int id);
        int UpdateEntertainment(EntertainmentSubmitModel entity);
    }
}
